<?php

namespace QuatreCentQuatre\PHPPowerPoint;
use QuatreCentQuatre\PHPPowerPoint\Presentation\Presentation;

/**
 * Class PHPPowerPoint
 * @package QuatreCentQuatre\PHPPowerPoint
 */
final class PHPPowerPoint {

    /** @var $presentation Presentation */
    private $presentation;

    public function __construct() {
        $this->presentation = new Presentation();
    }

    /**
     * Returns the presentation object of the PowerPoint.
     *
     * @return Presentation
     */
    public function getPresentation() {
        return $this->presentation;
    }

    /**
     * Set the presentation object of the PowerPoint.
     *
     * @param Presentation $presentation
     * @return $this
     * @throws \InvalidArgumentException
     */
    public function setPresentation(Presentation $presentation) {
        if (!($presentation instanceof Presentation))
            throw new \InvalidArgumentException("Invalid argument presentation.");

        $this->presentation = $presentation;
        return $this;
    }

    /**
     * Returns a new PowerPoint with only the given slides.
     *
     * @param array $slideList
     * @return PHPPowerPoint
     */
    public function slice($slideList = array()) {
        $doc = new PHPPowerPoint();
        $pres = new Presentation(false);
        foreach ($this->presentation->slideMasters() as $slideMaster) {
            $pres->addSlideMaster($slideMaster);
        }
        foreach ($slideList as $i) {
            $pres->addSlide($this->getPresentation()->getSlide($i));
        }
        return $doc->setPresentation($pres);
    }

    /**
     * Copy a list of slide from another document to this document.
     *
     * @param PHPPowerPoint $doc
     * @param array $slideList
     * @return $this
     */
    public function insertSlides(PHPPowerPoint $doc, $slideList = array()) {
        foreach ($slideList as $i) {
            $this->presentation->addSlide($doc->getPresentation()->getSlide($i));
        }
        return $this;
    }

    /**
     * Add all slides from given doc to the end of this document.
     *
     * @param PHPPowerPoint $doc
     * @return $this
     */
    public function merge(PHPPowerPoint $doc) {
        foreach ($doc->getPresentation()->slides() as $slide) {
            $this->presentation->addSlide($slide);
        }
        return $this;
    }
}