<?php


namespace QuatreCentQuatre\PHPPowerPoint;


use QuatreCentQuatre\PHPPowerPoint\Presentation\Identifier;

interface ISlide {
    public function getId();
    public function setId(Identifier $id);
} 