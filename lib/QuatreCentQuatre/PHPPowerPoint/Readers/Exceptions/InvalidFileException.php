<?php


namespace QuatreCentQuatre\PHPPowerPoint\Readers\Exceptions;


use QuatreCentQuatre\PHPPowerPoint\Exceptions\PHPPowerPointException;

/**
 * Class InvaliFileException
 * @package QuatreCentQuatre\PHPPowerPoint\Readers\Exceptions
 */
final class InvalidFileException extends PHPPowerPointException{
    const CODE = 0;

    public function __construct($message = "", $exception = null) {
        parent::__construct($message, $exception);
    }
} 