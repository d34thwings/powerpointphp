<?php

namespace QuatreCentQuatre\PHPPowerPoint\Readers\Exceptions;

use QuatreCentQuatre\PHPPowerPoint\Exceptions\PHPPowerPointException;

/**
 * Class CannotReadFileException
 * @package QuatreCentQuatre\PHPPowerPoint\Readers\Exceptions
 */
final class CannotReadFileException extends PHPPowerPointException{
    const CODE = 0;

    public function __construct($message = "", $exception = null) {
        parent::__construct($message, $exception);
    }
} 