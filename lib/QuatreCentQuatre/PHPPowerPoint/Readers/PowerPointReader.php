<?php

namespace QuatreCentQuatre\PHPPowerPoint\Readers;

use QuatreCentQuatre\PHPPowerPoint\Exceptions\PHPPowerPointException;
use QuatreCentQuatre\PHPPowerPoint\PHPPowerPoint;
use QuatreCentQuatre\PHPPowerPoint\Presentation\Builder\PresentationBuilder;
use QuatreCentQuatre\PHPPowerPoint\Presentation\Slide\Slide;
use QuatreCentQuatre\PHPPowerPoint\Presentation\Slide\SlideId;
use QuatreCentQuatre\PHPPowerPoint\Presentation\SlideLayout\Builder\SlideLayoutBuilder;
use QuatreCentQuatre\PHPPowerPoint\Presentation\SlideMaster\Builder\SlideMasterBuilder;
use QuatreCentQuatre\PHPPowerPoint\Presentation\SlideMaster\SlideMaster;
use QuatreCentQuatre\PHPPowerPoint\Presentation\SlideMaster\SlideMasterId;
use QuatreCentQuatre\PHPPowerPoint\Readers\Exceptions\CannotReadFileException;
use QuatreCentQuatre\PHPPowerPoint\Readers\Exceptions\InvalidFileException;
use XMLReader;
use ZipArchive;

final class PowerPointReader implements IReader
{

    public static function read($filename) {

        if (!file_exists($filename)) {
            throw new CannotReadFileException("File doesn't exist.");
        }

        $zip = new ZipArchive();

        if (!($zip->open($filename) === TRUE)) {
            throw new CannotReadFileException("Error while opening the file.");
        }

        // Open presentation files
        $presXML = self::stringToXMLReader(self::extractFile($zip, "ppt/presentation.xml"));
        $presRelationsXML = self::extractFile($zip, "ppt/_rels/presentation.xml.rels");

        // Create builders
        $presBuilder = new PresentationBuilder();
        $smb = new SlideMasterBuilder();
        $slb = new SlideLayoutBuilder();

        // Create new presentation
        $presBuilder->create();

        $count = 0;

        // Read presentation file
        while ($presXML->read()) {
            // Search for slide masters
            if ($presXML->name == "p:sldMasterId") {
                $count++;

                // Open slide master file
                $rel = self::getRelation($presRelationsXML, $presXML->getAttribute("r:id"));
                $masterXML = self::extractFile($zip, "ppt/" . $rel["Target"]);

                // Open slide master relation file
                $filename = explode('/', $rel["Target"])[1];
                $masterRelsXML = self::extractFile($zip, "ppt/slideMasters/_rels/" . $filename . '.rels');

                $master = SlideMaster::readFromXML(new XMLReaderWithRelations($masterXML, $masterRelsXML, $zip))
                    ->setId(new SlideMasterId($presXML->getAttribute("id")))
                    ->setName("Master " . $count);

                // Add the slide master to the presentation
                $presBuilder->withSlideMaster($master);
            }

            // Search for slides
            if ($presXML->name == "p:sldId") {
                // Open slide file
                $rel = self::getRelation($presRelationsXML, $presXML->getAttribute("r:id"));
                $slideXML = self::extractFile($zip, "ppt/" . $rel["Target"]);
                $e = explode('/', $rel["Target"]);
                $slideRelsXML = self::extractFile($zip, "ppt/slides/_rels/" . $e[count($e) - 1] . ".rels");

                $slide = Slide::readFromXML(new XMLReaderWithRelations($slideXML, $slideRelsXML, $zip))
                    ->setId(new SlideId($presXML->getAttribute("id")));

                // Add the slide to the presentation
                $presBuilder->withSlide($slide);
            }
        }

        $presXML->close();
        $zip->close();

        $doc = new PHPPowerPoint();
        $doc->setPresentation($presBuilder->now());
        return $doc;
    }

    /**
     * Retrieves a relation with the given id in the given xml.
     *
     * @param string $xml
     * @param int $rId
     * @throws \QuatreCentQuatre\PHPPowerPoint\Exceptions\PHPPowerPointException
     * @return array
     */
    public static function getRelation($xml, $rId) {
        $xmlReader = self::stringToXMLReader($xml);

        while ($xmlReader->read()) {
            if ($xmlReader->name == "Relationship" && $xmlReader->getAttribute("Id") == $rId) {
                return array(
                    "Id" => $xmlReader->getAttribute("Id"),
                    "Target" => $xmlReader->getAttribute("Target")
                );
            }
        }

        throw new PHPPowerPointException("Relation not found.");
    }

    /**
     * Extract a particular file from the ppt.
     *
     * @param ZipArchive $zip
     * @param string $path
     * @return string
     * @throws Exceptions\InvalidFileException
     */
    public static function extractFile($zip, $path) {
        $xml = $zip->getFromName($path);
        if (empty($xml)) {
            throw new InvalidFileException("File doesn't contain " . $path);
        }
        return $xml;
    }

    /**
     * Converts a string to an XML reader object.
     *
     * @param $xmlString
     * @return XMLReader
     */
    public static function stringToXMLReader($xmlString) {
        $xmlReader = new XMLReader();
        $xmlReader->XML($xmlString);
        return $xmlReader;
    }
}