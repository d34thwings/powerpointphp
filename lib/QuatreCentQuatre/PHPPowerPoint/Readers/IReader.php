<?php

namespace QuatreCentQuatre\PHPPowerPoint\Readers;

/**
 * Interface IReader
 * @package QuatreCentQuatre\PHPPowerPoint\Readers
 */
interface IReader {
    public static function read($filename);
}