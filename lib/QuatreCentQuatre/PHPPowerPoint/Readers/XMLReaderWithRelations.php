<?php


namespace QuatreCentQuatre\PHPPowerPoint\Readers;

use ZipArchive;

/**
 * Class that stores OpenXML with it's relations.
 *
 * @package QuatreCentQuatre\PHPPowerPoint\Readers
 */
final class XMLReaderWithRelations extends \XMLReader
{

    /** @var array */
    private $relations;

    /** @var ZipArchive */
    private $zip;

    /**
     * Constructor.
     *
     * @param string $xmlString
     * @param string $xmlRelsString
     * @param \ZipArchive $zip
     * @throws \InvalidArgumentException
     */
    public function __construct($xmlString, $xmlRelsString, ZipArchive $zip)
    {
        if (gettype($xmlString) != "string")
            throw new \InvalidArgumentException("Argument xmlString must be a string.");
        if ($xmlRelsString != null && gettype($xmlRelsString) != "string")
            throw new \InvalidArgumentException("Argument xmlRelsString must be a string.");

        $this->relations = array();
        $this->zip = $zip;

        if ($xmlRelsString != null) {
            $relsReader = PowerPointReader::stringToXMLReader($xmlRelsString);
            while ($relsReader->read()) {
                if ($relsReader->name == "Relationship") {
                    $this->relations[$relsReader->getAttribute("Id")] = $relsReader->getAttribute("Target");
                }
            }
        }

        parent::XML($xmlString);
    }

    /**
     * Returns the target corresponding to the given id.
     *
     * @param string $id
     * @return string
     * @throws \InvalidArgumentException
     */
    public function getRelation($id)
    {
        if (gettype($id) != "string")
            throw new \InvalidArgumentException("Argument id must be a string.");

        return $this->relations[$id];
    }

    /**
     * Returns all relations.
     *
     * @return array
     */
    public function relations()
    {
        return $this->relations;
    }

    /**
     * Returns image binary from relation id.
     *
     * @param string $id
     * @return string
     */
    public function getImageFromRelation($id)
    {
        $e = explode('/', $this->getRelation($id));
        $filename = $e[count($e) - 1];
        return $this->zip->getFromName("ppt/media/$filename");
    }

    /**
     * Returns the extension of an image from relation id.
     *
     * @param string $id
     * @return string
     */
    public function getImageFormatFromRelation($id)
    {
        $e = explode('/', $this->getRelation($id));
        $filename = $e[count($e) - 1];
        $e = explode('.', $filename);
        return $e[count($e) - 1];
    }

    /**
     * Returns a slide layout from relation id.
     *
     * @param string $id
     * @return string
     */
    public function getSlideLayoutFromRelation($id)
    {
        $e = explode('/', $this->getRelation($id));
        $filename = $e[count($e) - 1];
        return $this->zip->getFromName("ppt/slideLayouts/$filename");
    }

    /**
     * Creates a new reader from this reader with a different XML.
     *
     * @param string $source
     * @return XMLReaderWithRelations
     */
    public function subXML($source)
    {
        $xml = new XMLReaderWithRelations($source, null, $this->zip);
        $xml->relations = $this->relations;
        return $xml;
    }

    /**
     * Returns the current opened archive.
     *
     * @return ZipArchive
     */
    public function zip()
    {
        return $this->zip;
    }
} 