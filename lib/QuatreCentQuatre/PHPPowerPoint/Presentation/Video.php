<?php


namespace QuatreCentQuatre\PHPPowerPoint\Presentation;


use QuatreCentQuatre\PHPPowerPoint\Drawing\Transform2D;
use QuatreCentQuatre\PHPPowerPoint\Exceptions\PHPPowerPointException;
use QuatreCentQuatre\PHPPowerPoint\Readers\Exceptions\InvalidFileException;
use QuatreCentQuatre\PHPPowerPoint\Readers\XMLReaderWithRelations;

class Video {

    private $video;
    private $videoFormat;

    /**
     * Sets the video from a binary string.
     *
     * @param string $video
     * @param string $format
     * @throws \QuatreCentQuatre\PHPPowerPoint\Exceptions\PHPPowerPointException
     * @throws \InvalidArgumentException
     */
    public function setVideoFromBinary($video, $format) {
        if (gettype($video) != "string")
            throw new \InvalidArgumentException("Argument image must be of type string.");
        if (gettype($format) != "string")
            throw new \InvalidArgumentException("Argument format must be of type string.");
        if (array_search($format, ["mp4", "avi"]) === FALSE)
            throw new PHPPowerPointException("Video format $format is not supported.");
        $this->video = $video;
        $this->videoFormat = $format;
    }

    /**
     * Sets the video form a file path.
     *
     * @param string $path
     * @throws \QuatreCentQuatre\PHPPowerPoint\Exceptions\PHPPowerPointException
     * @throws \QuatreCentQuatre\PHPPowerPoint\Readers\Exceptions\InvalidFileException
     * @throws \InvalidArgumentException
     */
    public function setVideoFromFile($path) {
        if (gettype($path) != "string")
            throw new \InvalidArgumentException("Argument must be of type string.");
        if (!file_exists($path))
            throw new InvalidFileException("File $path doesn't exist.");

        $format = pathinfo($path, PATHINFO_EXTENSION);
        if (!array_search($format, ["mp4", "avi"]))
            throw new PHPPowerPointException("Image format $format is not supported.");

        $handle = fopen($path, 'rb');
        $image = fread($handle, filesize($path));
        fclose($handle);

        $this->video = $image;
        $this->videoFormat = $format;
    }

    /**
     * @return mixed
     */
    public function getVideo() {
        return $this->video;
    }
} 