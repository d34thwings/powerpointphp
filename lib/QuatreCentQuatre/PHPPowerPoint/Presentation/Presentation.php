<?php


namespace QuatreCentQuatre\PHPPowerPoint\Presentation;


use QuatreCentQuatre\PHPPowerPoint\Drawing\ParagraphProperties;
use QuatreCentQuatre\PHPPowerPoint\Exceptions\PHPPowerPointException;
use QuatreCentQuatre\PHPPowerPoint\Presentation\Slide\Slide;
use QuatreCentQuatre\PHPPowerPoint\Presentation\Slide\SlideId;
use QuatreCentQuatre\PHPPowerPoint\Presentation\SlideLayout\SlideLayout;
use QuatreCentQuatre\PHPPowerPoint\Presentation\SlideLayout\SlideLayoutId;
use QuatreCentQuatre\PHPPowerPoint\Presentation\SlideMaster\SlideMaster;
use QuatreCentQuatre\PHPPowerPoint\Presentation\SlideMaster\SlideMasterId;
use QuatreCentQuatre\PHPPowerPoint\Relationship\Relationship;

/**
 * Class Presentation
 * @package QuatreCentQuatre\PHPPowerPoint\Presentation
 */
final class Presentation {

    /** @var ParagraphProperties */
    private $defaultTheme;

    private $slideMasters;
    private $slides;

    private $images;
    private $writeMode = false;

    /**
     * Constructor
     */
    public function __construct($defaultTheme = true) {
        $this->slides = array();
        $this->slideMasters = array();
        if ($defaultTheme) {
            $this->useDefaultLayout();
        }
        $this->defaultTheme = new ParagraphProperties();
        $this->images = [];
    }

    /**
     * Add the default Office Theme to the presentation.
     */
    public function useDefaultLayout() {
        $master = new SlideMaster("Office Theme");
        $master->addSlideLayout(new SlideLayout("Title Slide"));
        $master->addSlideLayout(new SlideLayout("Title and Content"));
        $master->addSlideLayout(new SlideLayout("Section Header"));
        $master->addSlideLayout(new SlideLayout("Two Content"));
        $master->addSlideLayout(new SlideLayout("Comparison"));
        $master->addSlideLayout(new SlideLayout("Title Only"));
        $master->addSlideLayout(new SlideLayout("Blank"));
        $master->addSlideLayout(new SlideLayout("Content with Caption"));
        $master->addSlideLayout(new SlideLayout("Picture with Caption"));
        $master->addSlideLayout(new SlideLayout("Title and Vertical Text"));
        $master->addSlideLayout(new SlideLayout("Vertical Title and Text"));
        return $this->addSlideMaster($master);
    }

    /**
     * returns all the slide masters.
     *
     * @return array
     */
    public function slideMasters() {
        return $this->slideMasters;
    }

    /**
     * Returns a SlideMaster by it's index.
     *
     * @param $index
     * @return mixed
     * @throws \OutOfRangeException
     */
    public function getSlideMasterById($index) {
        if ($index >= count($this->slideMasters))
            throw new \OutOfRangeException("Index out of range.");

        return $this->slideMasters[$index];
    }

    /**
     * Adds a slide master to the presentation.
     *
     * @param SlideMaster $slideMaster
     * @param null $index
     * @throws \QuatreCentQuatre\PHPPowerPoint\Exceptions\PHPPowerPointException
     * @return $this
     */
    public function addSlideMaster(SlideMaster $slideMaster, $index = null) {
        foreach ($this->slideMasters as $m) {
            /* @var $m SlideMaster */
            if (strtolower($m->getName()) == strtolower($slideMaster->getName())) {
                throw new PHPPowerPointException("SlideMaster name must be unique within a Presentation.");
            }
        }

        $slideMaster->setPresentation($this);
        $count = count($this->slideMasters);

        if ($index != null && $index >= $count) {
            $index = null;
        }

        if ($index == null && gettype($index) != "integer") {
            $this->slideMasters[$count] = $slideMaster;
        } else {
            for ($i = $count; $i > $index; $i--) {
                $this->slideMasters[$i] = $this->slideMasters[$i - 1];
            }
            $this->slideMasters[$index] = $slideMaster;
        }

        return $this;
    }

    /**
     * Returns all the slides contained in this presentation.
     *
     * @return array
     */
    public function slides() {
        return $this->slides;
    }

    /**
     * Get a slide from it's index.
     *
     * @param int $index
     * @return Slide
     * @throws \OutOfRangeException
     */
    public function getSlide($index) {
        if ($index >= count($this->slides))
            throw new \OutOfRangeException("Index out of range.");

        return $this->slides[$index];
    }

    /**
     * Add a slide to the presentation.
     *
     * @param Slide $slide
     * @param int|null $slideLayout
     * @param int $index
     * @throws \QuatreCentQuatre\PHPPowerPoint\Exceptions\PHPPowerPointException
     * @return $this
     */
    public function addSlide(Slide $slide, $index = null, $slideLayout = null) {
        if ($slideLayout != null)
            $slide->setSlideLayout($slideLayout);

        $count = count($this->slides);

        if ($index != null && $index >= $count) {
            $index = null;
        }

        if ($index == null && gettype($index) != "integer") {
            $this->slides[$count] = $slide;
        } else {
            for ($i = $count; $i > $index; $i--) {
                $this->slides[$i] = $this->slides[$i - 1];
            }
            $this->slides[$index] = $slide;
        }

        $slide->setPresentation($this);

        return $this;
    }

    /**
     * Removes a slide by it's index.
     *
     * @param $index
     * @return $this
     * @throws \OutOfRangeException
     */
    public function removeSlide($index) {
        if ($index >= count($this->slides))
            throw new \OutOfRangeException("Index out of range.");

        unset($this->slides[$index]);

        for ($i = $index; $i < count($this->slides) - 1; $i++) {
            $this->slides[$i] = $this->slides[$i + 1];
        }

        unset($this->slides[count($this->slides) - 1]);

        return $this;
    }

    /**
     * Returns the default theme used to render texts.
     *
     * @return ParagraphProperties
     */
    public function getDefaultTheme()
    {
        return $this->defaultTheme;
    }

    /**
     * Add an image file to the presentation.
     * Available only from XML writing functions.
     *
     * @param $image
     * @param $format
     * @return string
     * @throws \QuatreCentQuatre\PHPPowerPoint\Exceptions\PHPPowerPointException
     */
    public function addImageFile($image, $format)
    {
        if (!$this->writeMode) throw new PHPPowerPointException('Function can only be called from writeToXML.');
        $filename = 'image'.(count($this->images) + 1).".$format";
        $this->images[$filename] = $image;
        return $filename;
    }

    /**
     * Clear image files from memory.
     *
     * @return $this
     */
    private function clearImageFiles()
    {
        $this->images = [];
        return $this;
    }

    ################################## XML ##################################

    const XML_NS_A = "http://schemas.openxmlformats.org/drawingml/2006/main";
    const XML_NS_P = "http://schemas.openxmlformats.org/presentationml/2006/main";
    const XML_NS_R = "http://schemas.openxmlformats.org/officeDocument/2006/relationships";

    /**
     * Returns the presentation.xml content.
     *
     * @param \ZipArchive $zip
     */
    public function writePresentationXML(\ZipArchive $zip) {
        $this->writeMode = true;
        $this->clearImageFiles();
        $relations = [];
        $j = 0;

        // Generating IDs for slides and slides masters
        foreach ($this->slideMasters as $i => $slideMaster) {
            /** @var $slideMaster SlideMaster */
            $slideMaster->setId(new SlideMasterId($i, true));

            // Generating IDs for slide layouts
            foreach ($slideMaster->layouts() as $slideLayout) {
                /** @var $slideLayout SlideLayout */
                $slideLayout->setId(new SlideLayoutId($j, true));
                $j++;
            }
        }
        foreach ($this->slides as $i => $slide) {
            /** @var $slide Slide */
            $slide->setId(new SlideId($i, true));
        }

        // Add default slide master
        foreach ($this->slideMasters as $slideMaster) {
            /** @var $slideMaster SlideMaster */
            $slideMaster->addSlideMasterToPowerPoint($zip);
        }

        // XML begin
        $writer = new \XMLWriter();
        $writer->openMemory();
        $writer->startDocument('1.0', 'UTF-8', 'yes');
        $writer->startElement('p:presentation');
        $writer->writeAttribute('xmlns:a', self::XML_NS_A);
        $writer->writeAttribute('xmlns:r', self::XML_NS_R);
        $writer->writeAttribute('xmlns:p', self::XML_NS_P);
        $writer->writeAttribute("saveSubsetFonts", 1);
        $writer->writeAttribute("autoCompressPictures", 0);
        // Add slide masters
        $writer->startElement("p:sldMasterIdLst");
        foreach ($this->slideMasters as $i => $master) {
            /** @var $master SlideMaster */
            $writer->startElement("p:sldMasterId");
            $writer->writeAttribute('id', $master->getId() . "");
            $count = count($relations) + 1;
            $j = $i + 1;
            $rel = new Relationship("rId$count", "slideMasters/slideMaster$j.xml", "http://schemas.openxmlformats.org/officeDocument/2006/relationships/slideMaster");
            $writer->writeAttribute('r:id', $rel->rId);
            $writer->endElement();
            $relations[] = $rel;
        }
        $writer->endElement();
        // Add slides
        $writer->startElement("p:sldIdLst");
        foreach ($this->slides as $i => $slide) {
            /** @var $slide Slide */
            $writer->startElement("p:sldId");
            $writer->writeAttribute('id', $slide->getId() . "");
            $count = count($relations) + 1;
            $j = $i + 1;
            $rel = new Relationship("rId$count", "slides/slide$j.xml", "http://schemas.openxmlformats.org/officeDocument/2006/relationships/slide");
            $writer->writeAttribute('r:id', $rel->rId);
            $writer->endElement();
            $relations[] = $rel;
        }
        $writer->endElement();
        $writer->startElement("p:sldSz");
        $writer->writeAttribute('cx', 9144000);
        $writer->writeAttribute('cy', 6858000);
        $writer->writeAttribute('type', "screen4x3");
        $writer->endElement();
        $writer->startElement("p:notesSz");
        $writer->writeAttribute('cx', 6858000);
        $writer->writeAttribute('cy', 9144000);
        $writer->endElement();
        $writer->writeRaw('<p:defaultTextStyle><a:defPPr><a:defRPr lang="en-US"/></a:defPPr><a:lvl1pPr marL="0" algn="l" defTabSz="457200" rtl="0" eaLnBrk="1" latinLnBrk="0" hangingPunct="1"><a:defRPr sz="1800" kern="1200"><a:solidFill><a:schemeClr val="tx1"/></a:solidFill><a:latin typeface="+mn-lt"/><a:ea typeface="+mn-ea"/><a:cs typeface="+mn-cs"/></a:defRPr></a:lvl1pPr><a:lvl2pPr marL="457200" algn="l" defTabSz="457200" rtl="0" eaLnBrk="1" latinLnBrk="0" hangingPunct="1"><a:defRPr sz="1800" kern="1200"><a:solidFill><a:schemeClr val="tx1"/></a:solidFill><a:latin typeface="+mn-lt"/><a:ea typeface="+mn-ea"/><a:cs typeface="+mn-cs"/></a:defRPr></a:lvl2pPr><a:lvl3pPr marL="914400" algn="l" defTabSz="457200" rtl="0" eaLnBrk="1" latinLnBrk="0" hangingPunct="1"><a:defRPr sz="1800" kern="1200"><a:solidFill><a:schemeClr val="tx1"/></a:solidFill><a:latin typeface="+mn-lt"/><a:ea typeface="+mn-ea"/><a:cs typeface="+mn-cs"/></a:defRPr></a:lvl3pPr><a:lvl4pPr marL="1371600" algn="l" defTabSz="457200" rtl="0" eaLnBrk="1" latinLnBrk="0" hangingPunct="1"><a:defRPr sz="1800" kern="1200"><a:solidFill><a:schemeClr val="tx1"/></a:solidFill><a:latin typeface="+mn-lt"/><a:ea typeface="+mn-ea"/><a:cs typeface="+mn-cs"/></a:defRPr></a:lvl4pPr><a:lvl5pPr marL="1828800" algn="l" defTabSz="457200" rtl="0" eaLnBrk="1" latinLnBrk="0" hangingPunct="1"><a:defRPr sz="1800" kern="1200"><a:solidFill><a:schemeClr val="tx1"/></a:solidFill><a:latin typeface="+mn-lt"/><a:ea typeface="+mn-ea"/><a:cs typeface="+mn-cs"/></a:defRPr></a:lvl5pPr><a:lvl6pPr marL="2286000" algn="l" defTabSz="457200" rtl="0" eaLnBrk="1" latinLnBrk="0" hangingPunct="1"><a:defRPr sz="1800" kern="1200"><a:solidFill><a:schemeClr val="tx1"/></a:solidFill><a:latin typeface="+mn-lt"/><a:ea typeface="+mn-ea"/><a:cs typeface="+mn-cs"/></a:defRPr></a:lvl6pPr><a:lvl7pPr marL="2743200" algn="l" defTabSz="457200" rtl="0" eaLnBrk="1" latinLnBrk="0" hangingPunct="1"><a:defRPr sz="1800" kern="1200"><a:solidFill><a:schemeClr val="tx1"/></a:solidFill><a:latin typeface="+mn-lt"/><a:ea typeface="+mn-ea"/><a:cs typeface="+mn-cs"/></a:defRPr></a:lvl7pPr><a:lvl8pPr marL="3200400" algn="l" defTabSz="457200" rtl="0" eaLnBrk="1" latinLnBrk="0" hangingPunct="1"><a:defRPr sz="1800" kern="1200"><a:solidFill><a:schemeClr val="tx1"/></a:solidFill><a:latin typeface="+mn-lt"/><a:ea typeface="+mn-ea"/><a:cs typeface="+mn-cs"/></a:defRPr></a:lvl8pPr><a:lvl9pPr marL="3657600" algn="l" defTabSz="457200" rtl="0" eaLnBrk="1" latinLnBrk="0" hangingPunct="1"><a:defRPr sz="1800" kern="1200"><a:solidFill><a:schemeClr val="tx1"/></a:solidFill><a:latin typeface="+mn-lt"/><a:ea typeface="+mn-ea"/><a:cs typeface="+mn-cs"/></a:defRPr></a:lvl9pPr></p:defaultTextStyle>');
        // XML end
        $writer->endElement();
        $writer->endDocument();

        $relations[] = new Relationship("rId".(count($relations) + 1), "presProps.xml", "http://schemas.openxmlformats.org/officeDocument/2006/relationships/presProps");
        $relations[] = new Relationship("rId".(count($relations) + 1), "tableStyles.xml", "http://schemas.openxmlformats.org/officeDocument/2006/relationships/viewProps");
        $relations[] = new Relationship("rId".(count($relations) + 1), "viewProps.xml", "http://schemas.openxmlformats.org/officeDocument/2006/relationships/theme");
        $relations[] = new Relationship("rId".(count($relations) + 1), "printerSettings/printerSettings1.bin", "http://schemas.openxmlformats.org/officeDocument/2006/relationships/printerSettings");
        $relations[] = new Relationship("rId".(count($relations) + 1), "theme/theme1.xml", "http://schemas.openxmlformats.org/officeDocument/2006/relationships/theme");

        $zip->addFromString("ppt/presentation.xml", $writer->outputMemory(TRUE));

        // presentation.xml.rels
        $writer = new \XMLWriter();
        $writer->openMemory();
        $writer->startDocument('1.0', 'UTF-8', 'yes');
        $writer->startElement("Relationships");
        $writer->writeAttribute("xmlns", "http://schemas.openxmlformats.org/package/2006/relationships");
        foreach ($relations as $rel) {
            /** @var $rel Relationship */
            $rel->writeToXML($writer);
        }
        $writer->endElement();
        $writer->endDocument();

        $zip->addFromString("ppt/_rels/presentation.xml.rels", $writer->outputMemory(TRUE));

        // Add slides to the XML
        foreach ($this->slides as $slide) {
            /** @var $slide Slide */
            $slide->addSlideToPowerPoint($zip);
        }

        // Images
        foreach ($this->images as $filename => $image) {
            $zip->addFromString("ppt/media/$filename", $image);
        }

        $this->clearImageFiles();
        $this->writeMode = false;
    }
} 