<?php


namespace QuatreCentQuatre\PHPPowerPoint\Presentation;

use QuatreCentQuatre\PHPPowerPoint\Drawing\Color;
use QuatreCentQuatre\PHPPowerPoint\Drawing\Gradient;
use QuatreCentQuatre\PHPPowerPoint\Drawing\Outline;
use QuatreCentQuatre\PHPPowerPoint\Drawing\Paragraph;
use QuatreCentQuatre\PHPPowerPoint\Drawing\ShapeLocks;
use QuatreCentQuatre\PHPPowerPoint\Drawing\Transform2D;
use QuatreCentQuatre\PHPPowerPoint\Exceptions\PHPPowerPointException;
use QuatreCentQuatre\PHPPowerPoint\IOpenXMLElement;
use QuatreCentQuatre\PHPPowerPoint\Readers\XMLReaderWithRelations;

final class Shape extends Element
{

    /** @var boolean */
    private $isTextBox = false;

    /** @var boolean */
    private $isList;

    /** @var string */
    private $spPreset;

    /** @var Transform2D */
    private $transform;

    private $placeholder_type;
    private $placeholder_idx;

    /** @var ShapeLocks */
    private $shape_locks;

    /** @var IOpenXMLElement */
    private $background;

    /** @var Outline */
    private $outline;

    /**
     * Constructor.
     *
     * @param string $preset
     * @param Transform2D $transform
     */
    public function __construct($preset = "rect", Transform2D $transform = null)
    {
        parent::__construct();
        $this->isTextBox(false);
    }

    /**
     * Returns wherever an Element is allowed as child or not.
     * Override this to edit allowed types.
     *
     * @param Element $e
     * @return bool
     */
    protected function isAllowed(Element $e)
    {
        $test = false;

        if ($e instanceof Paragraph)
            $test = true;

        return $test;
    }

    /**
     * Sets the shape to a text box or to a simple shape.
     *
     * @param $bool
     * @return $this
     * @throws \InvalidArgumentException
     */
    public function setTextBox($bool)
    {
        if (gettype($bool) != "boolean")
            throw new \InvalidArgumentException("Argument must be of type boolean.");

        if (!$bool)
            $this->isList = false;
        $this->isTextBox = $bool;
        return $this;
    }

    /**
     * Returns if the shape is a text box or not.
     *
     * @return mixed
     */
    public function isTextBox()
    {
        return $this->isTextBox;
    }

    /**
     * Sets the shape to a text box with a list style.
     *
     * @param $bool
     * @return $this
     * @throws \InvalidArgumentException
     */
    public function setList($bool)
    {
        if (gettype($bool) != "boolean")
            throw new \InvalidArgumentException("Argument must be of type boolean.");

        if ($bool)
            $this->isTextBox = true;
        $this->isList = $bool;
        return $this;
    }

    /**
     * Returns if the shape is a text box with a list style.
     *
     * @return mixed
     */
    public function isList()
    {
        return $this->isList;
    }

    /**
     * Returns the shape preset.
     *
     * @return string
     */
    public function getShapePreset()
    {
        return $this->spPreset;
    }

    /**
     * Sets the preset to use.
     *
     * @param string $preset
     * @return $this
     * @throws \InvalidArgumentException
     */
    public function setShapePreset($preset)
    {
        if (gettype($preset) != "string")
            throw new \InvalidArgumentException("Argument must be of type string.");

        $this->spPreset = $preset;
        return $this;
    }

    /**
     * Returns the position and size of the shape.
     *
     * @return Transform2D
     */
    public function getTransform2D()
    {
        return $this->transform;
    }

    /**
     * Sets the position and size of the shape.
     *
     * @param Transform2D $transform
     * @return $this
     * @throws \InvalidArgumentException
     */
    public function setTransform2D(Transform2D $transform)
    {
        if (!($transform instanceof Transform2D))
            throw new \InvalidArgumentException("Argument must be an object of type Transform2D.");

        $this->transform = $transform;
        return $this;
    }

    /**
     * Appends a paragraph to the shape.
     *
     * @param string $text
     * @param array $style
     * @return $this
     */
    public function addParagraph($text, $style = array())
    {
        return $this->append(new Paragraph($text, $style))->setTextBox(true);
    }

    /**
     * Sets the locks for the shape.
     *
     * @param array $locks
     */
    public function setShapeLocks($locks)
    {
        $this->shape_locks = new ShapeLocks($locks);
    }

    /**
     * Sets an outline with given style.
     *
     * @param $style
     * @return $this
     */
    public function setOutline($style)
    {
        $this->outline = new Outline($style);
        return $this;
    }

    /**
     * Returns the current background of the shape.
     *
     * @return IOpenXMLElement
     */
    public function getBackground()
    {
        return $this->background;
    }

    /**
     * Sets the background of the shape.
     *
     * @param IOpenXMLElement $background
     * @return $this
     * @throws \QuatreCentQuatre\PHPPowerPoint\Exceptions\PHPPowerPointException
     */
    public function setBackground(IOpenXMLElement $background)
    {
        // TODO : support image
        if (!($background instanceof Color) && !($background instanceof Gradient))
            throw new PHPPowerPointException('Background must be a Color or a Gradient.');

        $this->background = $background;
        return $this;
    }

    /**
     * Removes the current background of the shape.
     *
     * @return $this
     */
    public function removeBackground()
    {
        $this->background = null;
        return $this;
    }

    /**
     * Read a Shape from the given xml string.
     *
     * @param XMLReaderWithRelations $reader
     * @return mixed
     */
    public static function readFromXML(XMLReaderWithRelations $reader)
    {
        $shape = new Shape();

        $read = true;
        while ($read) {
            if ($reader->name == 'a:ln') {
                $shape->outline = Outline::readFromXML($reader->subXML($reader->readOuterXml()));
                $reader->next();
                continue;
            }
            if ($reader->name == 'a:solidFill') {
                $shape->background = Color::readFromXML($reader->subXML($reader->readOuterXml()));
                $reader->next();
                continue;
            }
            if ($reader->name == 'a:gradFill') {
                $shape->background = Gradient::readFromXML($reader->subXML($reader->readOuterXml()));
                $reader->next();
                continue;
            }
            if ($reader->name == 'a:spLocks') {
                $shape->shape_locks = ShapeLocks::readFromXML($reader->subXML($reader->readOuterXml()));
                $reader->next();
                continue;
            }
            if ($reader->name == "a:xfrm") {
                $shape->setTransform2D(Transform2D::readFromXML($reader->subXML($reader->readOuterXml())));
                $reader->next();
                continue;
            }
            if ($reader->name == "a:prstGeom") {
                $shape->setShapePreset($reader->getAttribute("prst"));
                $reader->next();
                continue;
            }
            if ($reader->name == "a:p") {
                $shape->isTextBox(true);
                $shape->append(Paragraph::readFromXML($reader->subXML($reader->readOuterXml())));
                $reader->next();
                continue;
            }
            if ($reader->name == "a:lstStyle") {
                $shape->isList(true);
            }
            if ($reader->name == 'p:ph') {
                $shape->placeholder_idx = $reader->getAttribute('idx');
                $shape->placeholder_type = $reader->getAttribute('type');
                $shape->spPreset = null;
            }

            $read = $reader->read();
        }

        return $shape;
    }

    /**
     * Writes an OpenXML to the XML writer.
     *
     * @param \XMLWriter $writer
     * @return mixed
     */
    function writeToXML(\XMLWriter $writer)
    {
        $writer->startElement("p:sp");
        $writer->startElement("p:nvSpPr");
        $writer->writeRaw('<p:cNvPr />');
        $writer->startElement('p:cNvSpPr');
        if ($this->isTextBox || $this->hasChildren())
            $writer->writeAttribute('txBox', true);
        if ($this->shape_locks != null)
            $this->shape_locks->writeToXML($writer);
        $writer->endElement();
        $writer->startElement('p:nvPr');
        if ($this->placeholder_idx != null || $this->placeholder_type != null) {
            $writer->startElement('p:ph');
            if ($this->placeholder_idx != null)
                $writer->writeAttribute('idx', $this->placeholder_idx);
            if ($this->placeholder_type != null)
                $writer->writeAttribute('type', $this->placeholder_type);
            $writer->endElement();
        }
        $writer->endElement();
        $writer->endElement();
        $writer->startElement("p:spPr");
        if ($this->transform != null)
            $this->transform->writeToXML($writer);
        if ($this->spPreset != null)
            $writer->writeRaw('<a:prstGeom prst="' . $this->spPreset . '"><a:avLst/></a:prstGeom>');
        if ($this->background != null)
            $this->background->writeToXML($writer);
        else
            $writer->writeElement("a:noFill");
        if ($this->outline != null)
            $this->outline->writeToXML($writer);
        $writer->endElement();
        $writer->startElement("p:txBody");
        if ($this->placeholder_idx != null || $this->placeholder_type != null)
            $writer->writeRaw('<a:bodyPr wrap="none" rtlCol="0"><a:spAutoFit/></a:bodyPr><a:lstStyle/>');
        else
            $writer->writeRaw('<a:bodyPr/>');
        foreach ($this->childElements as $child) {
            /** @var $child Element */
            $child->writeToXML($writer);
        }
        $writer->endElement();
        $writer->endElement();
    }
}