<?php


namespace QuatreCentQuatre\PHPPowerPoint\Presentation;


use QuatreCentQuatre\PHPPowerPoint\Drawing\Image;
use QuatreCentQuatre\PHPPowerPoint\Drawing\Transform2D;
use QuatreCentQuatre\PHPPowerPoint\Readers\XMLReaderWithRelations;

final class Picture extends Image {

    /** @var string */
    private $description;

    /** @var Transform2D */
    private $transform;

    /** @var string */
    private $spPreset;

    /** @var Video */
    private $video;

    /**
     * Constructor.
     *
     * @param string $name
     */
    public function __construct($name = "") {
        parent::__construct($name);
        $this->setShapePreset("rect");
        $this->setTransform2D(new Transform2D());
    }

    /**
     * Sets the image description.
     *
     * @param string $desc
     * @throws \InvalidArgumentException
     */
    public function setDescription($desc) {
        if (gettype($desc) != "string")
            throw new \InvalidArgumentException("Argument must be of type string.");

        $this->description = $desc;
    }

    /**
     * Returns the image description.
     *
     * @return string
     */
    public function getDescription() { return $this->description; }

    /**
     * Returns the shape preset.
     *
     * @return string
     */
    public function getShapePreset() {
        return $this->spPreset;
    }

    /**
     * Sets the preset to use.
     *
     * @param string $preset
     * @return $this
     * @throws \InvalidArgumentException
     */
    public function setShapePreset($preset) {
        if (gettype($preset) != "string")
            throw new \InvalidArgumentException("Argument must be of type string.");

        $this->spPreset = $preset;
        return $this;
    }

    /**
     * Returns the position and size of the shape.
     *
     * @return Transform2D
     */
    public function getTransform2D() {
        return $this->transform;
    }

    /**
     * Sets the position and size of the shape.
     *
     * @param Transform2D $transform
     * @return $this
     * @throws \InvalidArgumentException
     */
    public function setTransform2D(Transform2D $transform) {
        if (!($transform instanceof Transform2D))
            throw new \InvalidArgumentException("Argument must be an object of type Transform2D.");

        $this->transform = $transform;
        return $this;
    }

    /**
     * Sets a video for the picture.
     *
     * @param Video $video
     * @return $this
     * @throws \InvalidArgumentException
     */
    public function setVideo(Video $video) {
        if (!($video instanceof Video))
            throw new \InvalidArgumentException("Argument must be an object of type Video.");
        $this->video = $video;
        return $this;
    }

    /**
     * returns if the picture has a video.
     *
     * @return bool
     */
    public function hasVideo() {
        return $this->video != null;
    }

    /**
     * Returns the picture's video.
     *
     * @return Video
     */
    public function getVideo() {
        return $this->video;
    }

    /**
     * Read a Picture from the given xml string.
     *
     * @param \QuatreCentQuatre\PHPPowerPoint\Readers\XMLReaderWithRelations $reader
     * @return Picture
     */
    public static function readFromXML(XMLReaderWithRelations $reader) {
        $pic = new Picture();

        $read = true;
        while ($read) {
            if ($reader->name == "a:xfrm") {
                $pic->setTransform2D(Transform2D::readFromXML($reader->subXML($reader->readOuterXml())));
                $reader->next();
                continue;
            }
            if ($reader->name == "a:prstGeom") {
                $pic->setShapePreset($reader->getAttribute("prst"));
                $reader->next();
                continue;
            }
            if ($reader->name == "a:videoFile") {
                $video = new Video();
                $video->setVideoFromBinary(
                    $reader->getImageFromRelation($reader->getAttribute("r:link")),
                    $reader->getImageFormatFromRelation($reader->getAttribute("r:link"))
                );
            }
            if ($reader->name == "a:blip") {
                $pic->setImageFromBinary(
                    $reader->getImageFromRelation($reader->getAttribute("r:embed")),
                    $reader->getImageFormatFromRelation($reader->getAttribute("r:embed"))
                );
                $reader->next();
                continue;
            }

            $read = $reader->read();
        }

        return $pic;
    }

    /**
     * Writes an OpenXML to the XML writer.
     *
     * @param \XMLWriter $writer
     * @return mixed
     */
    function writeToXML(\XMLWriter $writer)
    {
        $writer->startElement('p:pic');
        $writer->startElement('p:nvPicPr');
        $writer->startElement('p:cNvPr');
        $writer->writeAttribute('name', $this->name);
        $writer->writeAttribute('descr', $this->description);
        $writer->endElement();
        $writer->writeRaw('<p:cNvPicPr><a:picLocks noChangeAspect="1"/></p:cNvPicPr>');
        $writer->writeElement('p:nvPr');
        $writer->endElement();
        parent::writeToXML($writer);
        $writer->startElement("p:spPr");
        $this->transform->writeToXML($writer);
        $writer->writeRaw('<a:prstGeom prst="'.$this->spPreset.'"><a:avLst/></a:prstGeom>');
        $writer->endElement();
        $writer->endElement();
    }
}