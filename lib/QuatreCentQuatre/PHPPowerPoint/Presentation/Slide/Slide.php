<?php


namespace QuatreCentQuatre\PHPPowerPoint\Presentation\Slide;

use QuatreCentQuatre\PHPPowerPoint\Drawing\Image;
use QuatreCentQuatre\PHPPowerPoint\Exceptions\PHPPowerPointException;
use QuatreCentQuatre\PHPPowerPoint\Presentation\AbstractSlide;
use QuatreCentQuatre\PHPPowerPoint\Presentation\Identifier;
use QuatreCentQuatre\PHPPowerPoint\Presentation\ShapeTree;
use QuatreCentQuatre\PHPPowerPoint\Readers\XMLReaderWithRelations;
use QuatreCentQuatre\PHPPowerPoint\Relationship\Relationship;

class Slide extends AbstractSlide
{
    protected $slideLayout;

    /**
     * Constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->slideLayout = 1;
    }

    /**
     * Sets the id of the slide layout.
     *
     * @param Identifier $id
     * @return $this
     * @throws \QuatreCentQuatre\PHPPowerPoint\Exceptions\PHPPowerPointException
     */
    public function setId(Identifier $id)
    {
        if (!($id instanceof SlideId))
            throw new PHPPowerPointException("Invalid identifier.");

        $this->id = $id;
        return $this;
    }

    /**
     * Sets the slide layout.
     *
     * @param int $id
     * @return $this
     * @throws \QuatreCentQuatre\PHPPowerPoint\Exceptions\PHPPowerPointException
     */
    public function setSlideLayout($id)
    {
        if (gettype($id) != "integer")
            throw new PHPPowerPointException("Invalid identifier.");

        $this->slideLayout = $id;
        return $this;
    }

    /**
     * Read a slide from the given xml string.
     *
     * @param \QuatreCentQuatre\PHPPowerPoint\Readers\XMLReaderWithRelations $reader
     * @return Slide
     */
    public static function readFromXML(XMLReaderWithRelations $reader)
    {
        $slide = new Slide();

        // Get the slide layout
        foreach ($reader->relations() as $rel) {
            /** @var $rel Relationship */
            if (!(strpos($rel, "slideLayouts") === FALSE)) {
                #$matches = array();
                preg_match_all('!\d+!', $rel, $matches);
                $slide->setSlideLayout(intval($matches[0][0]));
            }
        }

        $read = true;
        while ($read) {
            if ($reader->name == "p:cSld") {
                parent::readContentXML($reader->subXML($reader->readOuterXml()), $slide);
                $reader->next();
                continue;
            }
            $read = $reader->read();
        }
        return $slide;
    }

    /**
     * Writes an OpenXML to the XML writer.
     *
     * @param \XMLWriter $writer
     * @return mixed
     */
    public function writeToXML(\XMLWriter $writer)
    {
        $writer->startElement('p:sld');
        $writer->writeAttribute('xmlns:a', self::XML_NS_A);
        $writer->writeAttribute('xmlns:r', self::XML_NS_R);
        $writer->writeAttribute('xmlns:p', self::XML_NS_P);
        parent::writeContentToXML($writer);
        $writer->endElement();
    }

    /**
     * Adds the slide to the given PowerPoint archive.
     *
     * @param \ZipArchive $zip
     */
    public function addSlideToPowerPoint(\ZipArchive $zip)
    {
        $this->writeMode = true;

        $this->childRelations = [];
        $this->childRelations[] = new Relationship("rId" . (count($this->childRelations) + 1), "../slideLayouts/slideLayout$this->slideLayout.xml", "http://schemas.openxmlformats.org/officeDocument/2006/relationships/slideLayout");

        $id = $this->id->getWithoutOffset() + 1;

        // XML begin
        $writer = new \XMLWriter();
        $writer->openMemory();
        $writer->startDocument('1.0', 'UTF-8', 'yes');

        $this->writeToXML($writer);
        $writer->endDocument();
        $zip->addFromString("ppt/slides/slide$id.xml", $writer->outputMemory(TRUE));

        // slideX.xml.rels
        $writer = new \XMLWriter();
        $writer->openMemory();
        $writer->startDocument('1.0', 'UTF-8', 'yes');
        $writer->startElement("Relationships");
        $writer->writeAttribute("xmlns", "http://schemas.openxmlformats.org/package/2006/relationships");
        foreach ($this->childRelations as $rel) {
            /** @var $rel Relationship */
            $rel->writeToXML($writer);
        }
        $writer->endElement();
        $writer->endDocument();

        $zip->addFromString("ppt/slides/_rels/slide$id.xml.rels", $writer->outputMemory(TRUE));

        $this->writeMode = false;
    }
}