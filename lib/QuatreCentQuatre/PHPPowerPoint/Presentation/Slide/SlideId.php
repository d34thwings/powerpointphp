<?php


namespace QuatreCentQuatre\PHPPowerPoint\Presentation\Slide;


use QuatreCentQuatre\PHPPowerPoint\Presentation\Identifier;

class SlideId extends Identifier {

    public function __construct($id, $addOffset = false) {
        $this->START_INDEX = 256;
        parent::__construct($id, $addOffset);
    }

} 