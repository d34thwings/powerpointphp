<?php


namespace QuatreCentQuatre\PHPPowerPoint\Presentation\Slide\Builder;


use QuatreCentQuatre\PHPPowerPoint\IBuilder;
use QuatreCentQuatre\PHPPowerPoint\Presentation\ShapeTree;
use QuatreCentQuatre\PHPPowerPoint\Presentation\Slide\Slide;
use QuatreCentQuatre\PHPPowerPoint\Presentation\Slide\SlideId;

/**
 * Builder for Slide class.
 *
 * @package QuatreCentQuatre\PHPPowerPoint\Presentation\Slide\Builder
 */
final class SlideBuilder implements IBuilder {

    /** @var $slide Slide */
    private $slide;

    public function __construct() {
        $this->defaultName = "Default Slide Layout";
    }

    /**
     * Creates a new slide MasterObject.
     *
     * @return $this
     */
    public function create() {
        $this->slide = new Slide();
        return $this;
    }

    /**
     * Sets the id of the slide.
     *
     * @param $id
     * @return $this
     */
    public function withId($id) {
        $this->slide->setId(new SlideId($id));
        return $this;
    }

    /**
     * Adds a shape tree to the slide.
     *
     * @param ShapeTree $spTree
     * @return $this
     */
    public function withShapeTree(ShapeTree $spTree) {
        $this->slide->append($spTree);
        return $this;
    }

    /**
     * return the SlideMasterObject.
     *
     * @return Slide
     */
    public function now() {
        return $this->slide;
    }
}