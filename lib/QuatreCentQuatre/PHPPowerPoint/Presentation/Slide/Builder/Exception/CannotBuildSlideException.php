<?php


namespace QuatreCentQuatre\PHPPowerPoint\Presentation\Slide\Builder\Exception;


use QuatreCentQuatre\PHPPowerPoint\Exceptions\PHPPowerPointException;

/**
 * Class CannotBuildSlideException
 * @package QuatreCentQuatre\PHPPowerPoint\Presentation\Slide\Builder\Exception
 */
final class CannotBuildSlideException extends PHPPowerPointException {
    const CODE = 0;

    public function __construct($message = "", $exception = null) {
        parent::__construct($message, $exception);
    }
} 