<?php


namespace QuatreCentQuatre\PHPPowerPoint\Presentation;


use QuatreCentQuatre\PHPPowerPoint\Readers\XMLReaderWithRelations;

class ShapeTree extends Element {

    /**
     * Returns wherever an Element is allowed as child or not.
     * Override this to edit allowed types.
     *
     * @param Element $e
     * @return bool
     */
    protected function isAllowed(Element $e) {
        $test = false;

        if ($e instanceof Shape)
            $test = true;
        if ($e instanceof GroupShape)
            $test = true;
        if ($e instanceof Picture)
            $test = true;
        if ($e instanceof GraphicFrame)
            $test = true;

        return $test;
    }

    /**
     * Read a ShapeTree from the given xml string.
     *
     * @param XMLReaderWithRelations $reader
     * @return ShapeTree
     */
    public static function readFromXML(XMLReaderWithRelations $reader) {
        $spTree = new ShapeTree();

        $read = true;
        while ($read) {
            if ($reader->name == "p:sp") {
                $spTree->append(Shape::readFromXML($reader->subXML($reader->readOuterXml())));
                $reader->next();
                continue;
            }
            if ($reader->name == "p:pic") {
                $spTree->append(Picture::readFromXML($reader->subXML($reader->readOuterXml())));
                $reader->next();
                continue;
            }
            if ($reader->name == "p:grpSp") {
                $spTree->append(GroupShape::readFromXML($reader->subXML($reader->readOuterXml())));
                $reader->next();
                continue;
            }
            if ($reader->name == "p:graphicFrame") {
                $spTree->append(GraphicFrame::readFromXML($reader->subXML($reader->readOuterXml())));
                $reader->next();
                continue;
            }
            $read = $reader->read();
        }
        return $spTree;
    }

    /**
     * Writes an OpenXML to the XML writer.
     *
     * @param \XMLWriter $writer
     * @return mixed
     */
    function writeToXML(\XMLWriter $writer)
    {
        $writer->startElement("p:spTree");
        $writer->writeRaw('<p:nvGrpSpPr><p:cNvPr id="1" name=""/><p:cNvGrpSpPr/><p:nvPr/></p:nvGrpSpPr><p:grpSpPr><a:xfrm><a:off x="0" y="0"/><a:ext cx="0" cy="0"/><a:chOff x="0" y="0"/><a:chExt cx="0" cy="0"/></a:xfrm></p:grpSpPr>');
        foreach ($this->childElements as $child) {
            /** @var $child Element */
            $child->writeToXML($writer);
        }
        $writer->endElement();
    }
}