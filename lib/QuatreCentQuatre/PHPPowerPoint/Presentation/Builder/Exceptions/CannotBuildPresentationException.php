<?php


namespace QuatreCentQuatre\PHPPowerPoint\Presentation\Builder\Exceptions;


use QuatreCentQuatre\PHPPowerPoint\Exceptions\PHPPowerPointException;

/**
 * Class CannotBuildPresentationException
 * @package QuatreCentQuatre\PHPPowerPoint\Presentation\Builders\Exceptions
 */
final class CannotBuildPresentationException extends PHPPowerPointException{
    const CODE = 0;

    public function __construct($message = "", $exception = null) {
        parent::__construct($message, $exception);
    }
} 