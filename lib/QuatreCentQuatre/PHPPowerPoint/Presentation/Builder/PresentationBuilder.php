<?php


namespace QuatreCentQuatre\PHPPowerPoint\Presentation\Builder;


use OutOfRangeException;
use QuatreCentQuatre\PHPPowerPoint\IBuilder;
use QuatreCentQuatre\PHPPowerPoint\Presentation\Builder\Exceptions\CannotBuildPresentationException;
use QuatreCentQuatre\PHPPowerPoint\Presentation\Presentation;
use QuatreCentQuatre\PHPPowerPoint\Presentation\Slide\Slide;
use QuatreCentQuatre\PHPPowerPoint\Presentation\SlideMaster\SlideMaster;

/**
 * Builder for the Presentation class.
 *
 * @package QuatreCentQuatre\PHPPowerPoint\Presentation\Builders
 */
final class PresentationBuilder implements IBuilder{

    /**
     * @var Presentation
     */
    private $presentation;

    public function __construct() {

    }

    /**
     * Creates a new Presentation object.
     *
     * @return $this
     */
    public function create() {
        $this->presentation = new Presentation(false);
        return $this;
    }

    /**
     * Adds the default theme to the presentation.
     *
     * @return $this
     */
    public function withDefaultLayout() {
        $this->presentation->useDefaultLayout();
        return $this;
    }

    /**
     * Adds a slide master to the created Presentation object.
     *
     * @param SlideMaster $slideMaster
     * @return $this
     */
    public function withSlideMaster(SlideMaster $slideMaster) {
        $this->presentation->addSlideMaster($slideMaster);
        return $this;
    }

    /**
     * Adds a slide to the created Presentation object.
     *
     * @param Slide $slide
     * @param null $index
     * @return $this
     */
    public function withSlide(Slide $slide, $index = null) {
        $this->presentation->addSlide($slide, $index);
        return $this;
    }

    /**
     * Returns the Presentation if valid.
     *
     * @throws CannotBuildPresentationException
     * @return Presentation
     */
    public function now() {
        try {
            $this->presentation->getSlideMasterById(0);
        } catch (OutOfRangeException $e) {
            throw new CannotBuildPresentationException("Presentation must have at least one SlideMaster");
        }

        try {
            $this->presentation->getSlide(0);
        } catch (OutOfRangeException $e) {
            throw new CannotBuildPresentationException("Presentation must have at least one Slide");
        }

        return $this->presentation;
    }
} 