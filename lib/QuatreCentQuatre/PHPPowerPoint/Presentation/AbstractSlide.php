<?php


namespace QuatreCentQuatre\PHPPowerPoint\Presentation;

use QuatreCentQuatre\PHPPowerPoint\Drawing\Color;
use QuatreCentQuatre\PHPPowerPoint\Drawing\Gradient;
use QuatreCentQuatre\PHPPowerPoint\Drawing\Image;
use QuatreCentQuatre\PHPPowerPoint\Drawing\Table;
use QuatreCentQuatre\PHPPowerPoint\Drawing\Transform2D;
use QuatreCentQuatre\PHPPowerPoint\Exceptions\PHPPowerPointException;
use QuatreCentQuatre\PHPPowerPoint\IOpenXMLElement;
use QuatreCentQuatre\PHPPowerPoint\ISlide;
use QuatreCentQuatre\PHPPowerPoint\Readers\XMLReaderWithRelations;
use QuatreCentQuatre\PHPPowerPoint\Relationship\Relationship;

abstract class AbstractSlide extends Element implements ISlide {

    /** @var Identifier */
    protected $id;

    protected $transition;
    protected $timeout;

    /** @var array */
    protected $childRelations;

    protected $writeMode = false;

    /** @var IOpenXMLElement */
    protected $background;

    /**
     * Constructor.
     */
    public function __construct($name = "")
    {
        parent::__construct($name);
        $this->id = null;
        $this->childRelations = [];
    }

    /**
     * Returns wherever an Element is allowed as child or not.
     * Override this to edit allowed types.
     *
     * @param Element $e
     * @return bool
     */
    protected final function isAllowed(Element $e)
    {
        $test = false;

        if ($e instanceof ShapeTree)
            $test = true;

        return $test;
    }

    /**
     * Returns the id of the slide layout.
     *
     * @return Identifier
     */
    public final function getId()
    {
        return $this->id;
    }

    /**
     * Sets the id of the slide layout.
     *
     * @param Identifier $id
     * @return $this
     * @throws \QuatreCentQuatre\PHPPowerPoint\Exceptions\PHPPowerPointException
     */
    public abstract function setId(Identifier $id);

    /**
     * Returns the transition effect used by this slide.
     *
     * @return string|null
     */
    public final function getTransition()
    {
        return $this->transition;
    }

    /**
     * Sets the transition effect to use.
     *
     * @param string $effect
     * @return $this
     * @throws \InvalidArgumentException
     */
    public final function setTransition($effect)
    {
        if (gettype($effect) != "string")
            throw new \InvalidArgumentException("Argument effect must be a string.");

        $this->transition = $effect;
        return $this;
    }

    /**
     * Removes the transition effect.
     *
     * @return $this
     */
    public final function removeTransition()
    {
        $this->transition = null;
        return $this;
    }

    /**
     * Returns the timeout before automatic slide change.
     *
     * @return mixed
     */
    public final function getTimeout()
    {
        return $this->timeout;
    }

    /**
     * Sets the slide timeout.
     *
     * @param int $timeout
     * @return $this
     * @throws \InvalidArgumentException
     */
    public final function setTimeout($timeout)
    {
        if (gettype($timeout) != "integer")
            throw new \InvalidArgumentException("Argument timeout must be an integer.");

        $this->timeout = $timeout;
        return $this;
    }

    /**
     * Removes the timeout.
     *
     * @return $this
     */
    public final function removeTimeout()
    {
        $this->timeout = null;
        return $this;
    }

    /**
     * Returns the shape tree of the slide.
     *
     * @return null|ShapeTree
     */
    public final function getShapeTree()
    {
        return $this->findOne("QuatreCentQuatre\\PHPPowerPoint\\Presentation\\ShapeTree");
    }

    /**
     * Adds a shape tree to the slide if one does not already exists.
     *
     * @return $this
     */
    public final function addShapeTree()
    {
        $spTree = $this->getShapeTree();
        if ($spTree == null)
            return $this->append(new ShapeTree());
        else
            return $this;
    }

    /**
     * Adds a shape to the slide.
     *
     * @param string $preset
     * @param Transform2D $transform
     * @return $this
     */
    public final function addShape($preset = "rect", Transform2D $transform = null)
    {
        return $this->getShapeTree()->append(new Shape($preset, $transform));
    }

    /**
     * Adds a text box to the slide.
     *
     * @param string $text
     * @param array $style
     * @param Transform2D $transform
     * @param string $preset
     * @return $this
     */
    public final function addTextBox($text, $style = array(), Transform2D $transform = null, $preset = "rect")
    {
        $shape = new Shape($preset, $transform);
        return $this->getShapeTree()->append($shape->addParagraph($text, $style));
    }

    /**
     * Adds a picture to the slide.
     *
     * @param string $path
     * @param Transform2D $transform
     * @return $this
     */
    public final function addImage($path, Transform2D $transform = null)
    {
        $img = new Picture();
        $this->getShapeTree()->append($img->setTransform2D($transform == null ? new Transform2D() : $transform)->setImageFromPath($path));
        return $this;
    }

    /**
     * Adds a table to the slide.
     *
     * @param Table $table
     * @param Transform2D $transform
     * @return $this
     */
    public final function addTable(Table $table, Transform2D $transform = null)
    {
        $gF = new GraphicFrame($transform);
        $this->getShapeTree()->append($gF->setGraphic($table));
        return $this;
    }

    /**
     * Returns the current background of the slide.
     *
     * @return IOpenXMLElement
     */
    public final function getBackground()
    {
        return $this->background;
    }

    /**
     * Sets the background of the slide.
     *
     * @param IOpenXMLElement $background
     * @return $this
     * @throws \QuatreCentQuatre\PHPPowerPoint\Exceptions\PHPPowerPointException
     */
    public final function setBackground(IOpenXMLElement $background)
    {
        if (!($background instanceof Color) && !($background instanceof Gradient) && !($background instanceof Image))
            throw new PHPPowerPointException('Background must be a Color or a Gradient or an Image.');

        if ($background instanceof Image)
            $background->parent = $this;

        $this->background = $background;
        return $this;
    }

    /**
     * Removes the current background of the slide.
     *
     * @return $this
     */
    public final function removeBackground()
    {
        $this->background = null;
        return $this;
    }


    /**
     * Clears all the content of the slide.
     *
     * @return $this
     */
    public final function clear()
    {
        for ($i = 0; $i < count($this->childElements); $i++) {
            unset($this->childElements[$i]);
        }
        return $this;
    }

    /**
     * Returns the presentation that contains this slide.
     *
     * @return Presentation|null
     */
    public final function getPresentation()
    {
        return $this->parent;
    }

    /**
     * Sets the presentation of the slide.
     *
     * @param Presentation $presentation
     * @return $this
     * @throws \InvalidArgumentException
     */
    public function setPresentation($presentation)
    {
        if (!($presentation instanceof Presentation))
            throw new \InvalidArgumentException('Argument must be of type presentation');
        $this->parent = $presentation;
        return $this;
    }

    /**
     * Stop propagation to parent element by returning the slide.
     *
     * @return $this|null|AbstractSlide
     */
    public final function getSlide()
    {
        return $this;
    }

    /**
     * Adds a relation to the slide for a child element.
     * Can only be called when slide is writing to XML.
     *
     * @param string $target
     * @param string $type
     * @param string|null $targetMode
     * @throws \QuatreCentQuatre\PHPPowerPoint\Exceptions\PHPPowerPointException
     * @return string
     */
    public final function addChildRelation($target, $type, $targetMode = null)
    {
        if ($this->writeMode === FALSE) throw new PHPPowerPointException('Function can only be called from writeToXML.');
        $rel = new Relationship("rId" . (count($this->childRelations) + 1), $target, $type, $targetMode);
        $this->childRelations[] = $rel;
        return $rel->rId;
    }

    /**
     * Adds an image relation to the slide for a child element.
     * Can only be called when slide is writing to XML.
     *
     * @param $image
     * @param $format
     * @throws \QuatreCentQuatre\PHPPowerPoint\Exceptions\PHPPowerPointException
     * @return string
     */
    public final function addImageRelation($image, $format)
    {
        if ($this->writeMode === FALSE) throw new PHPPowerPointException('Function can only be called from writeToXML.');
        $filename = $this->getPresentation()->addImageFile($image, $format);
        return $this->addChildRelation(
            "../media/$filename",
            'http://schemas.openxmlformats.org/officeDocument/2006/relationships/image'
        );
    }

    /**
     * Reads content of the slide.
     *
     * @param XMLReaderWithRelations $reader
     * @param AbstractSlide $slide
     * @return AbstractSlide
     */
    protected final static function readContentXML(XMLReaderWithRelations $reader, AbstractSlide $slide)
    {
        $read = true;
        while ($read) {
            if ($reader->name == "p:spTree") {
                $slide->append(ShapeTree::readFromXML($reader->subXML($reader->readOuterXml())));
                $reader->next();
                continue;
            }
            if ($reader->name == "p:transition") {
                $slide->setTimeout(intval($reader->getAttribute("advTm")));
                $reader->read();
                $slide->setTransition(substr($reader->name, 2));
                $reader->read();
            }
            if ($reader->name == "p:bgPr") {
                $reader->read();
                if ($reader->name == "a:blipFill") {
                    $slide->setBackground(Image::readFromXML($reader->subXML($reader->readOuterXml())));
                }
            }
            $read = $reader->read();
        }
        return $slide;
    }

    /**
     * Writes an OpenXML to the XML writer.
     *
     * @param \XMLWriter $writer
     * @return mixed
     */
    protected final function writeContentToXML(\XMLWriter $writer)
    {
        // Slide content
        $writer->startElement("p:cSld");
        if ($this->background != null) {
            $writer->startElement('p:bg');
            $writer->startElement('p:bgPr');
            if ($this->background instanceof Image)
                $this->background->writeToXMLWithDrawingNS($writer);
            else
                $this->background->writeToXML($writer);
            $writer->writeElement('a:effectLst');
            $writer->endElement();
            $writer->endElement();
        }
        foreach ($this->childElements as $child) {
            /** @var $child Element */
            $child->writeToXML($writer);
        }
        #$writer->writeRaw('<p:extLst><p:ext uri="{BB962C8B-B14F-4D97-AF65-F5344CB8AC3E}"><p14:creationId xmlns:p14="http://schemas.microsoft.com/office/powerpoint/2010/main" val="4000081986"/></p:ext></p:extLst>');
        $writer->endElement();
        $writer->startElement("p:clrMapOvr");
        $writer->writeElement("a:masterClrMapping");
        $writer->endElement();
    }
} 