<?php


namespace QuatreCentQuatre\PHPPowerPoint\Presentation\SlideLayout\Builder\Exceptions;


use QuatreCentQuatre\PHPPowerPoint\Exceptions\PHPPowerPointException;

/**
 * Class CannotBuildSlideLayoutException
 * @package QuatreCentQuatre\PHPPowerPoint\Presentation\SlideLayout\Builder\Exceptions
 */
final class CannotBuildSlideLayoutException extends PHPPowerPointException {
    const CODE = 0;

    public function __construct($message = "", $exception = null) {
        parent::__construct($message, $exception);
    }
} 