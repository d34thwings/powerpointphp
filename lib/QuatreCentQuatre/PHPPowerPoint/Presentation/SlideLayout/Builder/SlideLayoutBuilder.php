<?php


namespace QuatreCentQuatre\PHPPowerPoint\Presentation\SlideLayout\Builder;


use QuatreCentQuatre\PHPPowerPoint\IBuilder;
use QuatreCentQuatre\PHPPowerPoint\Presentation\SlideLayout\SlideLayout;
use QuatreCentQuatre\PHPPowerPoint\Presentation\SlideLayout\SlideLayoutId;

/**
 * Builder for SlideLayout class.
 *
 * @package QuatreCentQuatre\PHPPowerPoint\Presentation\SlideLayout\Builder
 */
final class SlideLayoutBuilder implements IBuilder {

    /** @var $slideLayout SlideLayout */
    private $slideLayout;

    private $defaultName;

    public function __construct() {
        $this->defaultName = "Default Slide Layout";
    }

    /**
     * Creates a new slide MasterObject.
     *
     * @return $this
     */
    public function create() {
        $this->slideLayout = new SlideLayout($this->defaultName);
        return $this;
    }

    /**
     * Sets the name of the SlideMaster.
     *
     * @param $name
     * @return $this
     */
    public function withName($name) {
        $this->slideLayout->setName($name);
        return $this;
    }

    /**
     * @param $id
     * @return $this
     */
    public function withId($id) {
        $this->slideLayout->setId(new SlideLayoutId($id));
        return $this;
    }

    /**
     * return the SlideMasterObject.
     *
     * @return SlideLayout
     */
    public function now() {
        return $this->slideLayout;
    }
}