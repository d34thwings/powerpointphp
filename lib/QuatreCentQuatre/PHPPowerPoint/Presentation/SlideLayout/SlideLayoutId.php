<?php


namespace QuatreCentQuatre\PHPPowerPoint\Presentation\SlideLayout;


use QuatreCentQuatre\PHPPowerPoint\Presentation\Identifier;

class SlideLayoutId extends Identifier {

    public function __construct($id, $addOffset = false) {
        $this->START_INDEX = 2147483648;
        parent::__construct($id, $addOffset);
    }
} 