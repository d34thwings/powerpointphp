<?php


namespace QuatreCentQuatre\PHPPowerPoint\Presentation\SlideLayout;


use QuatreCentQuatre\PHPPowerPoint\Exceptions\PHPPowerPointException;
use QuatreCentQuatre\PHPPowerPoint\Presentation\AbstractSlide;
use QuatreCentQuatre\PHPPowerPoint\Presentation\Identifier;
use QuatreCentQuatre\PHPPowerPoint\Presentation\SlideMaster\SlideMasterId;
use QuatreCentQuatre\PHPPowerPoint\Readers\XMLReaderWithRelations;
use QuatreCentQuatre\PHPPowerPoint\Relationship\Relationship;

class SlideLayout extends AbstractSlide {

    /**
     * Constructor.
     *
     * @param string $name
     * @throws \QuatreCentQuatre\PHPPowerPoint\Exceptions\PHPPowerPointException
     */
    public function __construct($name) {
        parent::__construct();
        $this->setName($name);
    }

    /**
     * Sets the id of the slide layout.
     *
     * @param Identifier $id
     * @return $this|void
     * @throws \QuatreCentQuatre\PHPPowerPoint\Exceptions\PHPPowerPointException
     */
    public function setId(Identifier $id) {
        if (!($id instanceof SlideLayoutId))
            throw new PHPPowerPointException("Invalid identifier.");

        $this->id = $id;
    }

    /**
     * Returns the name of the layout.
     *
     * @return string
     */
    public function getName() {
        return $this->name;
    }

    /**
     * Sets the name of the SlideLayout.
     *
     * @param $name
     * @throws \QuatreCentQuatre\PHPPowerPoint\Exceptions\PHPPowerPointException
     */
    public function setName($name) {
        if (empty($name) || gettype($name) != "string")
            throw new PHPPowerPointException("SlideLayout name is required.");

        $this->name = $name;
    }


    /**
     * Read a SlideLayout from the given xml string.
     *
     * @param XMLReaderWithRelations $reader
     * @return SlideLayout
     */
    public static function readFromXML(XMLReaderWithRelations $reader) {
        $layout = new SlideLayout("Temp name");

        // Read slide layout file
        while ($reader->read()) {
            if ($reader->name == "p:cSld") {
                $layout->setName($reader->getAttribute('name'));
                parent::readContentXML($reader->subXML($reader->readOuterXml()), $layout);
                $reader->next();
                continue;
            }
        }

        return $layout;
    }

    /**
     * Writes an OpenXML to the XML writer.
     *
     * @param \XMLWriter $writer
     * @return mixed
     */
    function writeToXML(\XMLWriter $writer)
    {
        $writer->startElement('p:sldLayout');
        $writer->writeAttribute('xmlns:a', self::XML_NS_A);
        $writer->writeAttribute('xmlns:r', self::XML_NS_R);
        $writer->writeAttribute('xmlns:p', self::XML_NS_P);
        parent::writeContentToXML($writer);
        $writer->endElement();
    }

    /**
     * Adds the slide layout to the given PowerPoint archive.
     *
     * @param \ZipArchive $zip
     * @param SlideMasterId $slideMasterId
     */
    public function addSlideLayoutToPowerPoint(\ZipArchive $zip, SlideMasterId $slideMasterId)
    {
        $this->writeMode = true;
        $this->childRelations = [];
        $id = $this->id->getWithoutOffset() + 1;
        $id2 = $slideMasterId->getWithoutOffset() + 1;

        // XML begin
        $writer = new \XMLWriter();
        $writer->openMemory();
        $writer->startDocument('1.0', 'UTF-8', 'yes');
        $this->writeToXML($writer);
        $writer->endDocument();

        $this->addChildRelation("../slideMasters/slideMaster$id2.xml", "http://schemas.openxmlformats.org/officeDocument/2006/relationships/slideMaster");
        $zip->addFromString("ppt/slideLayouts/slideLayout$id.xml", $writer->outputMemory(TRUE));

        // slideX.xml.rels
        $writer = new \XMLWriter();
        $writer->openMemory();
        $writer->startDocument('1.0', 'UTF-8', 'yes');
        $writer->startElement("Relationships");
        $writer->writeAttribute("xmlns", "http://schemas.openxmlformats.org/package/2006/relationships");
        foreach ($this->childRelations as $rel) {
            /** @var $rel Relationship */
            $rel->writeToXML($writer);
        }
        $writer->endElement();
        $writer->endDocument();

        $zip->addFromString("ppt/slideLayouts/_rels/slideLayout$id.xml.rels", $writer->outputMemory(TRUE));

        $this->writeMode = false;
    }
}