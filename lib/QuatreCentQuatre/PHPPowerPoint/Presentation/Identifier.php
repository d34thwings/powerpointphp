<?php


namespace QuatreCentQuatre\PHPPowerPoint\Presentation;

/**
 * Class Identifier
 * @package QuatreCentQuatre\PHPPowerPoint
 */
abstract class Identifier
{

    protected $START_INDEX = 0;

    protected $id = 0;

    /**
     * Constructor
     *
     * @param $id
     * @param bool $addOffset
     */
    public function __construct($id, $addOffset = false)
    {
        if ($addOffset) {
            $this->set($this->START_INDEX + $id);
        } else {
            $this->set($id);
        }
    }

    /**
     * Sets the identifier number.
     *
     * @param $id
     * @throws \InvalidArgumentException
     */
    public function set($id)
    {
        if ($id < $this->START_INDEX)
            throw new \InvalidArgumentException("The id must be strictly greater than " . $this->$START_INDEX);

        $this->id = $id;
    }

    /**
     * Gets the identifier number.
     *
     * @return int
     */
    public final function get()
    {
        return $this->id;
    }

    /**
     * Gets the identifier number minus the START_OFFSET.S
     *
     * @return int
     */
    public final function getWithoutOffset()
    {
        return $this->id - $this->START_INDEX;
    }

    /**
     * Converts id to string.
     *
     * @return string
     */
    public function __toString()
    {
        return $this->id . "";
    }

} 