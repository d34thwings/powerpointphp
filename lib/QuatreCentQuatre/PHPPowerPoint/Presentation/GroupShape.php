<?php


namespace QuatreCentQuatre\PHPPowerPoint\Presentation;


use QuatreCentQuatre\PHPPowerPoint\Readers\XMLReaderWithRelations;

class GroupShape extends Element {

    /**
     * Returns wherever an Element is allowed as child or not.
     * Override this to edit allowed types.
     *
     * @param Element $e
     * @return bool
     */
    protected function isAllowed(Element $e) {
        $test = false;

        if ($e instanceof Shape)
            $test = true;
        if ($e instanceof GroupShape)
            $test = true;
        if ($e instanceof Picture)
            $test = true;

        return $test;
    }

    /**
     * Read a GroupShape from the given xml string.
     *
     * @param \QuatreCentQuatre\PHPPowerPoint\Readers\XMLReaderWithRelations $reader
     * @return mixed
     */
    public static function readFromXML(XMLReaderWithRelations $reader) {
        $grpSp = new GroupShape();

        $read = true;
        while ($read) {
            if ($reader->name == "p:sp") {
                $grpSp->append(Shape::readFromXML($reader->subXML($reader->readOuterXml())));
                $reader->next();
                continue;
            }
            if ($reader->name == "p:pic") {
                $grpSp->append(Picture::readFromXML($reader->subXML($reader->readOuterXml())));
                $reader->next();
                continue;
            }
            if ($reader->name == "p:grpSp") {
                $grpSp->append(GroupShape::readFromXML($reader->subXML($reader->readOuterXml())));
                $reader->next();
                continue;
            }
            $read = $reader->read();
        }
        return $grpSp;
    }

    /**
     * Writes an OpenXML to the XML writer.
     *
     * @param \XMLWriter $writer
     * @return mixed
     */
    function writeToXML(\XMLWriter $writer)
    {
        // TODO: Implement writeToXML() method.
    }
}