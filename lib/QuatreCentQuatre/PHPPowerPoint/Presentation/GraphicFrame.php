<?php


namespace QuatreCentQuatre\PHPPowerPoint\Presentation;


use QuatreCentQuatre\PHPPowerPoint\Drawing\Table;
use QuatreCentQuatre\PHPPowerPoint\Drawing\Transform2D;
use QuatreCentQuatre\PHPPowerPoint\Readers\XMLReaderWithRelations;

class GraphicFrame extends Element {

    /** @var Transform2D */
    private $transform;

    /** @var Element */
    private $graphic;

    /**
     * Constructor.
     *
     * @param Transform2D $transform
     */
    public function __construct(Transform2D $transform = null) {
        parent::__construct();
        if ($transform != null)
            $this->setTransform2D($transform);
        else
            $this->setTransform2D(new Transform2D());
    }

    /**
     * Returns wherever an Element is allowed as child or not.
     * Override this to edit allowed types.
     *
     * @param Element $e
     * @return bool
     */
    protected function isAllowed(Element $e) {
        return false;
    }

    /**
     * Returns the position and size of the shape.
     *
     * @return Transform2D
     */
    public function getTransform2D() {
        return $this->transform;
    }

    /**
     * Sets the position and size of the shape.
     *
     * @param Transform2D $transform
     * @return $this
     * @throws \InvalidArgumentException
     */
    public function setTransform2D(Transform2D $transform) {
        if (!($transform instanceof Transform2D))
            throw new \InvalidArgumentException("Argument must be an object of type Transform2D.");

        $this->transform = $transform;
        return $this;
    }

    /**
     * Returns the graphic element contained in the frame.
     *
     * @return Element
     */
    public function getGraphic() {
        return $this->graphic;
    }

    /**
     * Sets the graphic frame content.
     *
     * @param Element $graphic
     * @return $this
     * @throws \InvalidArgumentException
     */
    public function setGraphic(Element $graphic) {
        if (!is_subclass_of($graphic, "QuatreCentQuatre\\PHPPowerPoint\\Presentation\\Element"))
            throw new \InvalidArgumentException("Argument must be an Element.");
        $this->graphic = $graphic;
        return $this;
    }

    /**
     * Removes the graphic element contained in the  frame.
     *
     * @return $this
     */
    public function removeGraphic() {
        unset($this->graphic);
        return $this;
    }

    /**
     * Adds a table to the graphic frame. Returns the created table.
     *
     * @param int $rows
     * @param int $columns
     * @return Table
     * @throws \InvalidArgumentException
     */
    public function addTable($rows = 1, $columns = 1) {
        if ($rows < 1)
            throw new \InvalidArgumentException("Number of rows must be greater or equal to 1.");
        if ($columns < 1)
            throw new \InvalidArgumentException("Number of columns must be greater or equal to 1.");
        $table = new Table($rows, $columns);
        $this->append($table);
        return $table;
    }

    /**
     * Read a GraphicFrame from the given xml string.
     *
     * @param \QuatreCentQuatre\PHPPowerPoint\Readers\XMLReaderWithRelations $reader
     * @return mixed
     */
    public static function readFromXML(XMLReaderWithRelations $reader) {
        $gF = new GraphicFrame();

        $read = true;
        while($read) {
            if ($reader->name == "p:xfrm") {
                $gF->setTransform2D(Transform2D::readFromXML($reader->subXML($reader->readOuterXml())));
                $reader->next();
                continue;
            }
            if ($reader->name == "a:tbl") {
                $gF->setGraphic(Table::readFromXML($reader->subXML($reader->readOuterXml())));
                $reader->next();
                continue;
            }
            $read = $reader->read();
        }

        return $gF;
    }

    /**
     * Writes an OpenXML to the XML writer.
     *
     * @param \XMLWriter $writer
     * @return mixed
     */
    function writeToXML(\XMLWriter $writer)
    {
        $writer->startElement('p:graphicFrame');
        $writer->startElement('p:nvGraphicFramePr');
        $writer->writeElement('p:cNvPr');
        $writer->writeRaw('<p:cNvGraphicFramePr><a:graphicFrameLocks noGrp="1"/></p:cNvGraphicFramePr>');
        $writer->writeElement('p:nvPr');
        $writer->endElement();
        $this->transform->writeToXMLWithPresentationNS($writer);
        $writer->startElement('a:graphic');
        $this->graphic->writeToXML($writer);
        $writer->endElement();
        $writer->endElement();
    }
}