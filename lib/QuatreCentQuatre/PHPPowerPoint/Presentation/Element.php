<?php


namespace QuatreCentQuatre\PHPPowerPoint\Presentation;


use QuatreCentQuatre\PHPPowerPoint\Exceptions\PHPPowerPointException;
use QuatreCentQuatre\PHPPowerPoint\IOpenXMLElement;
use QuatreCentQuatre\PHPPowerPoint\Presentation\Slide\Slide;

abstract class Element implements IOpenXMLElement {

    /** @var array[Element] */
    protected $childElements;

    protected $name;
    protected $attributes;

    /** @var Element */
    protected $parent;

    public function __construct($name = "") {
        $this->childElements = array();
        $this->name = $name;
        $this->attributes = array();
    }

    /**
     * Cloning function.
     */
    public function __clone() {
        foreach ($this->childElements as $i => $e) {
            $this->childElements[$i] = clone $e;
            $this->childElements[$i]->parent = $this;
        }
    }

    /**
     * Destructor.
     */
    public function __destruct() {
        for ($i = 0; $i < count($this->childElements); $i++) {
            unset($this->childElements[$i]);
        }
    }

    /**
     * Returns wherever an Element is allowed as child or not.
     * Override this to edit allowed types.
     *
     * @param Element $e
     * @return bool
     */
    protected abstract function isAllowed(Element $e);

    /**
     * Inserts element at given Index.
     *
     * @param int $index
     * @param Element $e
     * @return $this
     * @throws \QuatreCentQuatre\PHPPowerPoint\Exceptions\PHPPowerPointException
     */
    public final function insertAt($index, Element $e) {
        if (!$this->isAllowed($e))
            throw new PHPPowerPointException("Element not allowed as child.");

        $count = count($this->childElements);

        if ($index != null && $index >= $count) {
            $index = null;
        }

        if ($index == null) {
            $this->childElements[] = $e;
        } else {
            array_splice($this->childElements, $index, 0, [$e]);
        }

        $e->parent = $this;
        return $this;
    }

    /**
     * Inserts element after the given $ref element.
     *
     * @param Element $ref
     * @param Element $e
     * @return $this
     * @throws \QuatreCentQuatre\PHPPowerPoint\Exceptions\PHPPowerPointException
     */
    public final function insertAfter(Element $ref, Element $e) {
        $i = array_search($ref, $this->childElements);
        if ($i === FALSE)
            throw new PHPPowerPointException("Element not found.");
        return $this->insertAt($i + 1, $e);
    }

    /**
     * Inserts element at index 0.
     *
     * @param Element $e
     * @return $this
     */
    public final function append(Element $e) {
        return $this->insertAt(null, $e);
    }

    /**
     * Inserts element after last child element.
     *
     * @param Element $e
     * @return $this
     */
    public final function prepend(Element $e) {
        return $this->insertAt(0, $e);
    }

    /**
     * Removes the given element from children.
     *
     * @param Element $element
     * @return $this
     * @throws \QuatreCentQuatre\PHPPowerPoint\Exceptions\PHPPowerPointException
     */
    public final function removeChild(Element $element) {
        $i = array_search($element, $this->childElements);
        if ($i === FALSE)
            throw new PHPPowerPointException("Element not found.");
        array_splice($this->childElements, $i, 1);
        return $this;
    }

    /**
     * Removes a child element by its name.
     *
     * @param string $name
     * @return $this
     * @throws \QuatreCentQuatre\PHPPowerPoint\Exceptions\PHPPowerPointException
     */
    public final function removeChildByName($name) {
        foreach ($this->childElements as $i => $e) {
            /** @var $e Element */
            if ($e->name == $name) {
                $this->removeChild($e);
                return $this;
            }
        }

        throw new PHPPowerPointException("Element with name $name not found.");
    }

    /**
     * Returns if this element contains child elements.
     *
     * @return bool
     */
    public final function hasChildren() {
        return count($this->childElements) > 0;
    }

    /**
     * Returns the first child element or null if there's no children.
     *
     * @return null|Element
     */
    public final function getFirstChild() {
        if ($this->hasChildren())
            return $this->childElements[0];
        return null;
    }

    /**
     * Returns the last child element or null if there's no children.
     *
     * @return null|Element
     */
    public final function getLastChild() {
        if ($this->hasChildren())
            return $this->childElements[count($this->childElements) - 1];
        return null;
    }

    /**
     * Returns the child with the specified index.
     *
     * @param int $index
     * @return Element|null
     */
    public final function getChild($index) {
        return $this->childElements[$index];
    }

    /**
     * Returns the index of the given child element.
     *
     * @param Element $element
     * @return int|null
     */
    public final function getChildIndex(Element $element) {
        return array_search($element, $this->childElements);
    }

    /**
     * Returns true if the given attribute is set.
     *
     * @param string $key
     * @return bool
     */
    public final function hasAttribute($key) {
        return isset($this->attributes[$key]);
    }

    /**
     * Returns the value of the given attribute.
     *
     * @param string $key
     * @return mixed
     * @throws \QuatreCentQuatre\PHPPowerPoint\Exceptions\PHPPowerPointException
     */
    public final function getAttribute($key) {
        if (!$this->hasAttribute($key))
            throw new PHPPowerPointException("Attribute $key dosen't exist.");
        return $this->attributes[$key];
    }

    /**
     * Sets the given attributes.
     *
     * @param string $key
     * @param mixed $value
     * @return $this
     */
    public final function setAttribute($key, $value) {
        $this->attributes[$key] = $value;
        return $this;
    }

    /**
     * Removes the given attribute.
     *
     * @param string $key
     * @return $this
     * @throws \QuatreCentQuatre\PHPPowerPoint\Exceptions\PHPPowerPointException
     */
    public final function removeAttribute($key) {
        if (!$this->hasAttribute($key))
            throw new PHPPowerPointException("Attribute $key dosen't exist.");
        unset($this->attributes[$key]);
        return $this;
    }

    /**
     * Returns all the child elements.
     *
     * @return array
     */
    public final function children() {
        return $this->childElements;
    }

    /**
     * Returns the parent element if exists.
     *
     * @return null|Element
     */
    public final function parent() {
        return $this->parent();
    }

    /**
     * Finds one object of the given type.
     *
     * @param $type
     * @return Element|null
     */
    public final function findOne($type) {
        foreach ($this->childElements as $element) {
            if (get_class($element) == $type) {
                return $element;
            }
        }
        return null;
    }

    /**
     * Returns all the objects of given type.
     *
     * @param $type
     * @return array
     */
    public final function find($type) {
        $results = array();
        foreach ($this->childElements as $element) {
            if (get_class($element) == $type) {
                $results[] = $element;
            }
        }
        return $results;
    }

    /**
     * Returns the presentation that contain this element.
     *
     * @return Presentation|null
     */
    public function getPresentation() {
        return $this->parent != null ? $this->parent->getPresentation() : null;
    }

    /**
     * Returns the slide containing this element.
     *
     * @return Slide|null
     */
    public function getSlide() {
        return $this->parent != null ? $this->parent->getSlide() : null;
    }
} 