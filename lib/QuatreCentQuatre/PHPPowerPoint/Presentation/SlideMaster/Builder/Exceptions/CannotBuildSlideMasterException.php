<?php


namespace QuatreCentQuatre\PHPPowerPoint\Presentation\SlideMaster\Builder\Exceptions;


use QuatreCentQuatre\PHPPowerPoint\Exceptions\PHPPowerPointException;

/**
 * Class CannotBuildSlideMasterException
 * @package QuatreCentQuatre\PHPPowerPoint\Presentation\Builders\Exceptions
 */
final class CannotBuildSlideMasterException extends PHPPowerPointException {
    const CODE = 0;

    public function __construct($message = "", $exception = null) {
        parent::__construct($message, $exception);
    }
} 