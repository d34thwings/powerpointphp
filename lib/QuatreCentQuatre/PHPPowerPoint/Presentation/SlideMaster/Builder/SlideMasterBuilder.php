<?php


namespace QuatreCentQuatre\PHPPowerPoint\Presentation\SlideMaster\Builder;


use QuatreCentQuatre\PHPPowerPoint\IBuilder;
use QuatreCentQuatre\PHPPowerPoint\Presentation\SlideLayout\SlideLayout;
use QuatreCentQuatre\PHPPowerPoint\Presentation\SlideMaster\Builder\Exceptions\CannotBuildSlideMasterException;
use QuatreCentQuatre\PHPPowerPoint\Presentation\SlideMaster\SlideMaster;
use QuatreCentQuatre\PHPPowerPoint\Presentation\SlideMaster\SlideMasterId;

/**
 * Builder for SlideMaster class.
 *
 * @package QuatreCentQuatre\PHPPowerPoint\Presentation\SlideMaster\Builder
 */
final class SlideMasterBuilder implements IBuilder {

    /** @var $slideMaster SlideMaster */
    private $slideMaster;

    private $defaultName;

    public function __construct() {
        $this->defaultName = "Default Slide Master";
    }

    /**
     * Creates a new slide MasterObject.
     *
     * @return $this
     */
    public function create() {
        $this->slideMaster = new SlideMaster($this->defaultName);
        return $this;
    }

    /**
     * Sets the name of the SlideMaster.
     *
     * @param $name
     * @return $this
     */
    public function withName($name) {
        $this->slideMaster->setName($name);
        return $this;
    }

    /**
     * Add a layout to the SlideMaster.
     *
     * @param SlideLayout $layout
     * @param null $index
     * @return $this
     */
    public function withLayout(SlideLayout $layout, $index = null) {
        $this->slideMaster->addSlideLayout($layout, $index);
        return $this;
    }

    /**
     * @param $id
     * @return $this
     */
    public function withId($id) {
        $this->slideMaster->setId(new SlideMasterId($id));
        return $this;
    }

    /**
     * return the SlideMasterObject.
     *
     * @return SlideMaster
     * @throws Exceptions\CannotBuildSlideMasterException
     */
    public function now() {
        try {
            $this->slideMaster->getLayoutByIndex(0);
        } catch (\OutOfRangeException $e) {
            throw new CannotBuildSlideMasterException("SlideMaster must have at least one SlideLayout");
        }

        return $this->slideMaster;
    }
}