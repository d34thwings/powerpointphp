<?php


namespace QuatreCentQuatre\PHPPowerPoint\Presentation\SlideMaster;


use QuatreCentQuatre\PHPPowerPoint\Exceptions\PHPPowerPointException;
use QuatreCentQuatre\PHPPowerPoint\Presentation\AbstractSlide;
use QuatreCentQuatre\PHPPowerPoint\Presentation\Identifier;
use QuatreCentQuatre\PHPPowerPoint\Presentation\Presentation;
use QuatreCentQuatre\PHPPowerPoint\Presentation\SlideLayout\SlideLayout;
use QuatreCentQuatre\PHPPowerPoint\Readers\PowerPointReader;
use QuatreCentQuatre\PHPPowerPoint\Readers\XMLReaderWithRelations;
use QuatreCentQuatre\PHPPowerPoint\Relationship\Relationship;

class SlideMaster extends AbstractSlide
{

    private $slideLayouts;

    /**
     * Constructor
     *
     * @param string $name
     * @throws \QuatreCentQuatre\PHPPowerPoint\Exceptions\PHPPowerPointException
     */
    public function __construct($name)
    {
        parent::__construct();
        $this->setName($name);
        $this->slideLayouts = array();
    }

    /**
     * Sets the id of the slide master.
     *
     * @param Identifier $id
     * @return $this
     * @throws \QuatreCentQuatre\PHPPowerPoint\Exceptions\PHPPowerPointException
     */
    public function setId(Identifier $id)
    {
        if (!($id instanceof SlideMasterId))
            throw new PHPPowerPointException("Invalid identifier.");

        $this->id = $id;
        return $this;
    }

    /**
     * Returns the unique name of the SlideMaster.
     *
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Sets the name of the SlideMaster.
     *
     * @param $name
     * @return $this
     * @throws \QuatreCentQuatre\PHPPowerPoint\Exceptions\PHPPowerPointException
     */
    public function setName($name)
    {
        if (empty($name) || gettype($name) != "string")
            throw new PHPPowerPointException("SlideMaster name is required.");

        $this->name = $name;
        return $this;
    }

    /**
     * Sets the presentation of the slide.
     *
     * @param Presentation $presentation
     * @return $this
     * @throws \InvalidArgumentException
     */
    public function setPresentation($presentation)
    {
        parent::setPresentation($presentation);
        foreach ($this->slideLayouts as $layout) {
            /** @var $layout SlideLayout */
            $layout->setPresentation($presentation);
        }
        return $this;
    }

    /**
     * Adds a SlideLayout to this SlideMaster. Names of the layouts must be unique.
     *
     * @param SlideLayout $layout
     * @param null $index
     * @throws \QuatreCentQuatre\PHPPowerPoint\Exceptions\PHPPowerPointException
     * @return $this
     */
    public function addSlideLayout(SlideLayout $layout, $index = null)
    {
        foreach ($this->slideLayouts as $l) {
            /* @var $l SlideLayout */
            if (strtolower($l->getName()) == strtolower($layout->getName())) {
                throw new PHPPowerPointException("SlideLayout name must be unique within a SlideMaster.");
            }
        }

        $count = count($this->slideLayouts);

        if ($index != null && $index >= $count) {
            $index = null;
        }

        if ($index == null) {
            $this->slideLayouts[] = $layout;
        } else {
            for ($i = $count; $i > $index; $i--) {
                $this->slideLayouts[$i] = $this->slideLayouts[$i - 1];
            }
            $this->slideLayouts[$index] = $layout;
        }

        return $this;
    }

    /**
     * Get a layout by it's index.
     *
     * @param int $index
     * @return SlideLayout
     * @throws \OutOfRangeException
     */
    public function getLayoutByIndex($index)
    {
        if ($index >= count($this->slideLayouts))
            throw new \OutOfRangeException("Index out of range.");

        return $this->slideLayouts[$index];
    }

    /**
     * Get a layout by it's name.
     *
     * @param string $name
     * @return SlideLayout
     * @throws \QuatreCentQuatre\PHPPowerPoint\Exceptions\PHPPowerPointException
     */
    public function getLayoutByName($name)
    {
        foreach ($this->slideLayouts as $l) {
            /* @var $l SlideLayout */
            if (strtolower($l->getName()) == strtolower($name)) {
                return $l;
            }
        }

        throw new PHPPowerPointException("Layout with name \"" . $name . "\" not found.");
    }

    /**
     * Removes a SlideLayout by it's index.
     *
     * @param $index
     * @return $this
     * @throws \OutOfRangeException
     */
    public function removeLayoutById($index)
    {
        if ($index >= count($this->slideLayouts))
            throw new \OutOfRangeException("Index out of range.");

        unset($this->slideLayouts[$index]);

        for ($i = $index; $i < count($this->slideLayouts) - 1; $i++) {
            $this->slideLayouts[$i] = $this->slideLayouts[$i + 1];
        }

        unset($this->slideLayouts[count($this->slideLayouts) - 1]);

        return $this;
    }

    /**
     * Removes a SlideLayout by it's name.
     *
     * @param $name
     * @return $this
     * @throws \QuatreCentQuatre\PHPPowerPoint\Exceptions\PHPPowerPointException
     */
    public function removeLayoutByName($name)
    {
        foreach ($this->slideLayouts as $index => $l) {
            /* @var $l SlideLayout */
            if (strtolower($l->getName()) == strtolower($name)) {
                return $this->removeLayoutById($index);
            }
        }

        throw new PHPPowerPointException("Layout with name \"" . $name . "\" not found.");
    }

    /**
     * Returns the slide layouts contained in the slide master.
     *
     * @return array
     */
    public function layouts()
    {
        return $this->slideLayouts;
    }

    /**
     * Read a SlideMaster from the given xml string.
     *
     * @param XMLReaderWithRelations $reader
     * @return SlideMaster
     */
    public static function readFromXML(XMLReaderWithRelations $reader)
    {
        $master = new SlideMaster("Temp name");

        $read = true;
        while ($read) {
            if ($reader->name == "p:cSld") {
                parent::readContentXML($reader->subXML($reader->readOuterXml()), $master);
                $reader->next();
                continue;
            }
            if ($reader->name == "p:sldLayoutId") {
                // Create new layout
                $reader->getAttribute("id");

                // Open layout file
                $filename = explode('/', $reader->getRelation($reader->getAttribute("r:id")))[2];
                $layoutXML = $reader->getSlideLayoutFromRelation($reader->getAttribute("r:id"));

                $layoutRelsXML = PowerPointReader::extractFile($reader->zip(), "ppt/slideLayouts/_rels/" . $filename . '.rels');
                $layout = SlideLayout::readFromXML(new XMLReaderWithRelations($layoutXML, $layoutRelsXML, $reader->zip()));

                // Add the layout to the slide master
                $master->addSlideLayout($layout);
            }
            $read = $reader->read();
        }

        return $master;
    }

    public function writeToXML(\XMLWriter $writer)
    {
        $writer->startElement('p:sldMaster');
        $writer->writeAttribute('xmlns:a', self::XML_NS_A);
        $writer->writeAttribute('xmlns:r', self::XML_NS_R);
        $writer->writeAttribute('xmlns:p', self::XML_NS_P);
        parent::writeContentToXML($writer);
        $writer->startElement('p:sldLayoutIdLst');
        foreach ($this->slideLayouts as $layout) {
            /** @var $layout SlideLayout */
            $writer->startElement('p:sldLayoutId');
            $writer->writeAttribute('id', $layout->getId() . "");
            $id = $layout->getId()->getWithoutOffset() + 1;
            $rId = $this->addChildRelation("../slideLayouts/slideLayout$id.xml", "http://schemas.openxmlformats.org/officeDocument/2006/relationships/slideLayout");
            $writer->writeAttribute('r:id', $rId);
            $writer->endElement();
        }
        $writer->endElement();
        $writer->endElement();
    }

    /**
     * Adds the slide master to the given PowerPoint archive.
     *
     * @param \ZipArchive $zip
     */
    public function addSlideMasterToPowerPoint(\ZipArchive $zip)
    {
        $this->writeMode = true;
        $this->childRelations = [];
        $id = $this->id->getWithoutOffset() + 1;

        // XML begin
        $writer = new \XMLWriter();
        $writer->openMemory();
        $writer->startDocument('1.0', 'UTF-8', 'yes');
        $this->writeToXML($writer);
        $writer->endDocument();

        $this->addChildRelation("../theme/theme1.xml", "http://schemas.openxmlformats.org/officeDocument/2006/relationships/theme");
        $zip->addFromString("ppt/slideMasters/slideMaster$id.xml", $writer->outputMemory(TRUE));

        // slideX.xml.rels
        $writer = new \XMLWriter();
        $writer->openMemory();
        $writer->startDocument('1.0', 'UTF-8', 'yes');
        $writer->startElement("Relationships");
        $writer->writeAttribute("xmlns", "http://schemas.openxmlformats.org/package/2006/relationships");
        foreach ($this->childRelations as $rel) {
            /** @var $rel Relationship */
            $rel->writeToXML($writer);
        }
        $writer->endElement();
        $writer->endDocument();

        $zip->addFromString("ppt/slideMasters/_rels/slideMaster$id.xml.rels", $writer->outputMemory(TRUE));

        foreach ($this->slideLayouts as $slideLayout) {
            /** @var $slideLayout SlideLayout */
            $slideLayout->addSlideLayoutToPowerPoint($zip, $this->id);
        }

        $this->writeMode = false;
    }
}