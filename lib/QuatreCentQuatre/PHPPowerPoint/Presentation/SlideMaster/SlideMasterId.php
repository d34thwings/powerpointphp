<?php


namespace QuatreCentQuatre\PHPPowerPoint\Presentation\SlideMaster;


use QuatreCentQuatre\PHPPowerPoint\Presentation\Identifier;

class SlideMasterId extends Identifier {

    public function __construct($id, $addOffset = false) {
        $this->START_INDEX = 2147483648;
        parent::__construct($id, $addOffset);
    }
} 