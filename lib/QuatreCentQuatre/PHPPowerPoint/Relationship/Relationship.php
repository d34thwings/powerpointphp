<?php


namespace QuatreCentQuatre\PHPPowerPoint\Relationship;


final class Relationship {

    public $rId;
    public $target;
    public $type;
    public $targetMode;

    /**
     * Constructor.
     *
     * @param string $rId
     * @param string $target
     * @param string $type
     * @param string|null $targetMode
     */
    public function __construct($rId, $target, $type, $targetMode = null) {
        $this->rId = $rId;
        $this->target = $target;
        $this->type = $type;
        $this->targetMode = $targetMode;
    }

    /**
     * Writes the relation into an opened XMLWriter.
     *
     * @param \XMLWriter $writer
     */
    public function writeToXML(\XMLWriter $writer) {
        $writer->startElement("Relationship");
        $writer->writeAttribute("Id", $this->rId);
        $writer->writeAttribute("Type", $this->type);
        $writer->writeAttribute("Target", $this->target);
        if ($this->targetMode != null)
            $writer->writeAttribute("TargetMode", $this->targetMode);
        $writer->endElement();
    }

} 