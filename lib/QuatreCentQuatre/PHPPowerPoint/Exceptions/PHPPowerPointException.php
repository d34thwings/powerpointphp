<?php


namespace QuatreCentQuatre\PHPPowerPoint\Exceptions;


class PHPPowerPointException extends \Exception {
    const CODE = 0;

    /**
     * Basic PHPPowerPoint exception.
     *
     * @param string $message
     * @param \Exception $exception
     */
    public function __construct($message = "", \Exception $exception = null) {
        parent::__construct($message, self::CODE, $exception);
    }

} 