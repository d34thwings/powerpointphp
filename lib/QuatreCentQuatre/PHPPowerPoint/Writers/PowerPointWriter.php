<?php


namespace QuatreCentQuatre\PHPPowerPoint\Writers;


use QuatreCentQuatre\PHPPowerPoint\PHPPowerPoint;
use QuatreCentQuatre\PHPPowerPoint\Presentation\Presentation;
use QuatreCentQuatre\PHPPowerPoint\Presentation\SlideLayout\SlideLayout;
use QuatreCentQuatre\PHPPowerPoint\Presentation\SlideMaster\SlideMaster;
use XMLWriter;
use ZipArchive;

final class PowerPointWriter {

    /**
     * @param PHPPowerPoint $doc
     * @param string $destination
     * @param bool $overwrite
     * @return bool
     */
    public static function write(PHPPowerPoint $doc, $destination, $overwrite = true) {
        $zip = new ZipArchive();
        if($zip->open($destination,$overwrite ? ZIPARCHIVE::OVERWRITE : ZIPARCHIVE::CREATE) !== true) {
            return false;
        }

        $files = array();

        // Add default theme
        $zip->addFile(__DIR__."/Resources/ppt/theme/theme1.xml", "ppt/theme/theme1.xml");

        // Add root relation file
        $zip->addFile(__DIR__."/Resources/_rels/.rels", "_rels/.rels");

        // Add presProps.xml, tableStyles.xml and viewProps.xml
        $zip->addFile(__DIR__."/Resources/ppt/presProps.xml", "ppt/presProps.xml");
        $zip->addFile(__DIR__."/Resources/ppt/tableStyles.xml", "ppt/tableStyles.xml");
        $zip->addFile(__DIR__."/Resources/ppt/viewProps.xml", "ppt/viewProps.xml");

        // Add printer settings
        $zip->addFile(__DIR__."/Resources/ppt/printerSettings/printerSettings1.bin", "ppt/printerSettings/printerSettings1.bin");

        // Add empty thumbnail
        $zip->addFile(__DIR__."/Resources/docProps/thumbnail.jpeg", "docProps/thumbnail.jpeg");

        // app.xml
        $appXML = new XMLWriter();
        $appXML->openMemory();
        $appXML->startDocument('1.0', 'UTF-8', 'yes');
        $appXML->startElement('Properties');
        $appXML->writeAttribute("xmlns", "http://schemas.openxmlformats.org/officeDocument/2006/extended-properties");
        $appXML->writeAttribute("xmlns:vt", "http://schemas.openxmlformats.org/officeDocument/2006/docPropsVTypes");
        $appXML->writeElement("TotalTime", 0);
        $appXML->writeElement("Words", 0);
        $appXML->writeElement("Application", "PHP PHPPowerPoint");
        $appXML->writeElement("PresentationFormat", "On-screen Show (4:3)");
        $appXML->writeElement("Paragraphs", 0);
        $appXML->writeElement("Slides", count($doc->getPresentation()->slides()));
        $appXML->writeElement("Notes", 0);
        $appXML->writeElement("HiddenSlides", 0);
        $appXML->writeElement("MMClips", 0);
        $appXML->writeElement("ScaleCrop", "false");
        $appXML->startElement("HeadingPairs");
        $appXML->startElement("vt:vector");
        $appXML->writeAttribute("size", 4);
        $appXML->writeAttribute("baseType", "variant");
        $appXML->startElement("vt:variant");
        $appXML->writeElement("vt:lpstr", "Theme");
        $appXML->endElement();
        $appXML->startElement("vt:variant");
        $appXML->writeElement("vt:i4", count($doc->getPresentation()->slideMasters()));
        $appXML->endElement();
        $appXML->startElement("vt:variant");
        $appXML->writeElement("vt:lpstr", "Slide Titles");
        $appXML->endElement();
        $appXML->startElement("vt:variant");
        $appXML->writeElement("vt:i4", count($doc->getPresentation()->slides()));
        $appXML->endElement();
        $appXML->endElement();
        $appXML->endElement();
        $appXML->startElement("TitlesOfParts");
        $appXML->startElement("vt:vector");
        $appXML->writeAttribute("size", count($doc->getPresentation()->slides()) + count($doc->getPresentation()->slideMasters()));
        $appXML->writeAttribute("baseType", "lpstr");
        foreach ($doc->getPresentation()->slideMasters() as $slideMaster) {
            /** @var $slideMaster SlideMaster */
            $appXML->writeElement("vt:lpstr", $slideMaster->getName());
        }
        for ($i = 0; $i < count($doc->getPresentation()->slides()); $i++)
            $appXML->writeElement("vt:lpstr", "PowerPoint Presentation");
        $appXML->endElement();
        $appXML->endElement();
        $appXML->writeElement("Company", "QuatreCentQuatre");
        $appXML->writeElement("LinksUpToDate", "false");
        $appXML->writeElement("SharedDoc", "false");
        $appXML->writeElement("HyperlinksChanged", "false");
        $appXML->writeElement("AppVersion", "14.0000");
        $appXML->endElement();
        $appXML->endDocument();

        $files["docProps/app.xml"] = $appXML->outputMemory(TRUE);

        // core.xml
        $coreXML = new XMLWriter();
        $coreXML->openMemory();
        $coreXML->startDocument('1.0', 'UTF-8', 'yes');
        $coreXML->startElement('cp:coreProperties');
        $coreXML->writeAttribute('xmlns:cp', "http://schemas.openxmlformats.org/package/2006/metadata/core-properties");
        $coreXML->writeAttribute('xmlns:dc', "http://purl.org/dc/elements/1.1/");
        $coreXML->writeAttribute('xmlns:dcterms', "http://purl.org/dc/terms/");
        $coreXML->writeAttribute('xmlns:dcmitype', "http://purl.org/dc/dcmitype/");
        $coreXML->writeAttribute('xmlns:xsi', "http://www.w3.org/2001/XMLSchema-instance");
        $coreXML->writeElement("dc:title", "PowerPoint Presentation");
        $coreXML->writeElement("dc:creator", "PHPPowerPoint");
        $coreXML->writeElement("cp:lastModifiedBy", "PHPPowerPoint");
        $coreXML->writeElement("cp:revision", "1");
        $coreXML->startElement("dcterms:created");
        $coreXML->writeAttribute('xsi:type', "dcterms:W3CDTF");
        $coreXML->writeRaw(date(DATE_ATOM));
        $coreXML->endElement();
        $coreXML->startElement("dcterms:modified");
        $coreXML->writeAttribute('xsi:type', "dcterms:W3CDTF");
        $coreXML->writeRaw(date(DATE_ATOM));
        $coreXML->endElement();
        $coreXML->endElement();
        $coreXML->endDocument();

        $files["docProps/core.xml"] = $coreXML->outputMemory(TRUE);

        // Update content types
        $contentTypesXML = new XMLWriter();
        $contentTypesXML->openMemory();
        $contentTypesXML->startDocument('1.0', 'UTF-8', 'yes');
        $contentTypesXML->startElement('Types');
        $contentTypesXML->writeAttribute("xmlns", "http://schemas.openxmlformats.org/package/2006/content-types");
        self::writeDefault($contentTypesXML, "xml", "application/xml");
        self::writeDefault($contentTypesXML, "bin", "application/vnd.openxmlformats-officedocument.presentationml.printerSettings");
        self::writeDefault($contentTypesXML, "rels", "application/vnd.openxmlformats-package.relationships+xml");
        self::writeDefault($contentTypesXML, "mp4", "video/unknown");
        self::writeDefault($contentTypesXML, "jpeg", "image/jpeg");
        self::writeDefault($contentTypesXML, "png", "image/png");
        self::writeOverride($contentTypesXML, "/ppt/presentation.xml", "application/vnd.openxmlformats-officedocument.presentationml.presentation.main+xml");
        self::writeOverride($contentTypesXML, "/ppt/presProps.xml", "application/vnd.openxmlformats-officedocument.presentationml.presProps+xml");
        self::writeOverride($contentTypesXML, "/ppt/viewProps.xml", "application/vnd.openxmlformats-officedocument.presentationml.viewProps+xml");
        self::writeOverride($contentTypesXML, "/ppt/theme/theme1.xml", "application/vnd.openxmlformats-officedocument.theme+xml");
        self::writeOverride($contentTypesXML, "/ppt/tableStyles.xml", "application/vnd.openxmlformats-officedocument.presentationml.tableStyles+xml");

        $i = 1;
        $j = 1;
        foreach ($doc->getPresentation()->slideMasters() as $slideMaster) {
            /** @var $slideMaster SlideMaster */
            self::writeOverride($contentTypesXML, "/ppt/slideMasters/slideMaster$i.xml", "application/vnd.openxmlformats-officedocument.presentationml.slideMaster+xml");
            foreach ($slideMaster->layouts() as $slideLayout) {
                /** @var $slideLayout SlideLayout */
                self::writeOverride($contentTypesXML, "/ppt/slideLayouts/slideLayout$j.xml", "application/vnd.openxmlformats-officedocument.presentationml.slideLayout+xml");
                $j++;
            }
            $i++;
        }
        foreach ($doc->getPresentation()->slides() as $i => $slide) {
            $j = $i + 1;
            self::writeOverride($contentTypesXML, "/ppt/slides/slide$j.xml", "application/vnd.openxmlformats-officedocument.presentationml.slide+xml");
        }

        self::writeOverride($contentTypesXML, "/docProps/core.xml", "application/vnd.openxmlformats-package.core-properties+xml");
        self::writeOverride($contentTypesXML, "/docProps/app.xml", "application/vnd.openxmlformats-officedocument.extended-properties+xml");
        $contentTypesXML->endElement();
        $contentTypesXML->endDocument();

        $files["[Content_Types].xml"] = $contentTypesXML->outputMemory(TRUE);

        // Write presentation to the XML
        $doc->getPresentation()->writePresentationXML($zip);

        // Add files to the archive
        foreach($files as $path => $file) {
            $zip->addFromString($path, $file);
        }
        $zip->close();
        return file_exists($destination);
    }

    private static function writeDefault(XMLWriter $writer, $extension, $contentType) {
        $writer->startElement("Default");
        $writer->writeAttribute("Extension", $extension);
        $writer->writeAttribute("ContentType", $contentType);
        $writer->endElement();
    }

    private static function writeOverride(XMLWriter $writer, $partName, $contentType) {
        $writer->startElement("Override");
        $writer->writeAttribute("PartName", $partName);
        $writer->writeAttribute("ContentType", $contentType);
        $writer->endElement();
    }
} 