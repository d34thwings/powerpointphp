<?php


namespace QuatreCentQuatre\PHPPowerPoint;


interface IBuilder {

    public function create();
    public function now();

} 