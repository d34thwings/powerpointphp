<?php


namespace QuatreCentQuatre\PHPPowerPoint\Drawing;


use QuatreCentQuatre\PHPPowerPoint\Presentation\Element;
use QuatreCentQuatre\PHPPowerPoint\Presentation\Picture;
use QuatreCentQuatre\PHPPowerPoint\Readers\XMLReaderWithRelations;

class Cell extends Element {

    /** @var boolean */
    private $isList;

    /** @var int */
    private $width;

    /**
     * Returns wherever an Element is allowed as child or not.
     * Override this to edit allowed types.
     *
     * @param Element $e
     * @return bool
     */
    protected function isAllowed(Element $e) {
        $test = false;

        if ($e instanceof Paragraph)
            $test = true;

        return $test;
    }

    /**
     * Sets the shape to a text box with a list style.
     *
     * @param $bool
     * @return $this
     * @throws \InvalidArgumentException
     */
    public function setList($bool) {
        if (gettype($bool) != "boolean")
            throw new \InvalidArgumentException("Argument must be of type boolean.");
        $this->isList = $bool;
        return $this;
    }

    /**
     * Returns if the shape is a text box with a list style.
     *
     * @return mixed
     */
    public function isList() {
        return $this->isList;
    }

    /**
     * Sets the width of the cell. The width of the cells of the first row are used for every other rows.
     *
     * @param int $width
     * @throws \OutOfBoundsException
     * @return $this
     */
    public function setWidth($width = 2032000) {
        if ($width < -27273042329600 || $width > 27273042329600)
            throw new \OutOfBoundsException("Height must be between -27273042329600 EMUs and 27273042329600 EMUs.");
        $this->width = $width;
        return $this;
    }

    /**
     * Return the width of the cell.
     *
     * @return int
     */
    public function getWidth() {
        return $this->width;
    }

    /**
     * Append some text with the given style to the cell.
     *
     * @param string $text
     * @param array $style
     * @return $this
     */
    public function appendText($text = "", $style = array()) {
        return $this->append(new Paragraph($text, $style));
    }

    /**
     * Read a Cell from the given xml string.
     *
     * @param \QuatreCentQuatre\PHPPowerPoint\Readers\XMLReaderWithRelations $reader
     * @return mixed
     */
    public static function readFromXML(XMLReaderWithRelations $reader) {
        $cell = new Cell();

        $read = true;
        while ($read) {
            if($reader->name == "a:p") {
                $cell->append(Paragraph::readFromXML($reader->subXML($reader->readOuterXml())));
                $reader->next();
                continue;
            }
            if($reader->name == "a:lstStyle") {
                $cell->isList(true);
            }

            $read = $reader->read();
        }
        return $cell;
    }

    /**
     * Writes an OpenXML to the XML writer.
     *
     * @param \XMLWriter $writer
     * @return mixed
     */
    function writeToXML(\XMLWriter $writer)
    {
        $writer->startElement('a:tc');
        $writer->startElement('a:txBody');
        $writer->writeElement('a:bodyPr');
        $writer->writeElement('a:lstStyle');
        foreach ($this->childElements as $child) {
            /** @var Element $child */
            $child->writeToXML($writer);
        }
        $writer->endElement();
        $writer->writeElement('a:tcPr');
        $writer->endElement();
    }
}