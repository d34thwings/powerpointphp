<?php


namespace QuatreCentQuatre\PHPPowerPoint\Drawing;


use QuatreCentQuatre\PHPPowerPoint\IOpenXMLElement;
use QuatreCentQuatre\PHPPowerPoint\Readers\XMLReaderWithRelations;

final class Transform2D implements IOpenXMLElement {

    /** @var Offset */
    private $off;

    /** @var Extents */
    private $ext;

    /** @var int */
    private $rot;

    const DEGREE_RATIO = 60000;

    /**
     * Constructor.
     *
     * @param null|\QuatreCentQuatre\PHPPowerPoint\Drawing\Offset $off
     * @param null|\QuatreCentQuatre\PHPPowerPoint\Drawing\Extents $ext
     * @param int $rot
     */
    public function __construct(Offset $off = null, Extents $ext = null, $rot = 0) {
        if ($off != null)
            $this->setOffset($off);
        else
            $this->setOffset(new Offset());
        if ($ext != null)
            $this->setExtents($ext);
        else
            $this->setExtents(new Extents());
        $this->rot = $rot;
    }

    /**
     * Returns the offset.
     *
     * @return Offset
     */
    public function getOffset() {
        return $this->off;
    }

    /**
     * Sets the offset.
     *
     * @param Offset $off
     * @return $this
     * @throws \InvalidArgumentException
     */
    public function setOffset(Offset $off) {
        if (!($off instanceof Offset))
            throw new \InvalidArgumentException("Argument must be an object of type Offset.");

        $this->off = $off;
        return $this;
    }

    /**
     * Returns the extents.
     *
     * @return Extents
     */
    public function getExtents() {
        return $this->ext;
    }

    /**
     * Sets the extents.
     *
     * @param Extents $ext
     * @return $this
     * @throws \InvalidArgumentException
     */
    public function setExtents(Extents $ext) {
        if (!($ext instanceof Extents))
            throw new \InvalidArgumentException("Argument must be an object of type Extents.");

        $this->ext = $ext;
        return $this;
    }

    /**
     * Returns the rotation angle in OpenXML degrees.
     *
     * @return int
     */
    public function getRotation() {
        return $this->rot;
    }

    /**
     * Sets the rotation in OpenXML degrees.
     *
     * @param int $rot
     * @return $this
     * @throws \InvalidArgumentException
     */
    public function setRotation($rot) {
        if (gettype($rot) != "integer")
            throw new \InvalidArgumentException("Argument must be an integer.");

        $this->rot = $rot;
        return $this;
    }

    /**
     * Converts degrees to OpenXML degrees.
     *
     * @param int|float $value
     * @return float
     */
    public static function degreesToOpenXMLDegrees($value) {
        return round($value * self::DEGREE_RATIO);
    }

    /**
     * Converts OpenXML degrees to degrees.
     *
     * @param int $value
     * @return float
     */
    public static function openXMLDegreesToDegrees($value) {
        return round($value / self::DEGREE_RATIO);
    }

    /**
     * Read a Transform2D from the given xml string.
     *
     * @param XMLReaderWithRelations $reader
     * @return mixed
     */
    public static function readFromXML(XMLReaderWithRelations $reader) {
        $transform = new Transform2D();

        $reader->read();
        $transform->setRotation(intval($reader->getAttribute('rot')));

        while ($reader->read()) {
            if ($reader->name == "a:off") {
                $transform->setOffset(Offset::readFromXML($reader->subXML($reader->readOuterXml())));
            }
            if ($reader->name == "a:ext") {
                $transform->setExtents(Extents::readFromXML($reader->subXML($reader->readOuterXml())));
            }
        }

        return $transform;
    }

    /**
     * Writes an OpenXML to the XML writer.
     *
     * @param \XMLWriter $writer
     * @return mixed
     */
    function writeToXML(\XMLWriter $writer)
    {
        $writer->startElement("a:xfrm");
        if ($this->rot != 0)
            $writer->writeAttribute('rot', $this->rot);
        $this->off->writeToXML($writer);
        $this->ext->writeToXML($writer);
        $writer->endElement();
    }

    /**
     * Writes an OpenXML to the XML writer.
     *
     * @param \XMLWriter $writer
     * @return mixed
     */
    function writeToXMLWithPresentationNS(\XMLWriter $writer)
    {
        $writer->startElement("p:xfrm");
        $this->off->writeToXML($writer);
        $this->ext->writeToXML($writer);
        $writer->endElement();
    }
}