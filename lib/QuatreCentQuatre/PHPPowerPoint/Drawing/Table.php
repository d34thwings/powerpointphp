<?php


namespace QuatreCentQuatre\PHPPowerPoint\Drawing;


use JsonSchema\Constraints\Format;
use QuatreCentQuatre\PHPPowerPoint\Presentation\Element;
use QuatreCentQuatre\PHPPowerPoint\Readers\XMLReaderWithRelations;

class Table extends Element {

    public function __construct($rows = 0, $columns = 0) {
        for ($i = 0; $i < $rows; $i++) {
            $this->append(new Row($columns));
        }
    }

    /**
     * Returns wherever an Element is allowed as child or not.
     * Override this to edit allowed types.
     *
     * @param Element $e
     * @return bool
     */
    protected function isAllowed(Element $e) {
        $test = false;

        if ($e instanceof Row)
            $test = true;

        return $test;
    }

    /**
     * Returns the number of rows contained in the table.
     *
     * @return int
     */
    public function rowCount() {
        return count($this->children());
    }

    /**
     * Returns the number of columns contained in the table.
     *
     * @return int
     */
    public function columnCount() {
        $firstRow = $this->getFirstChild();
        return $firstRow != null ? count($firstRow->children()) : 0;
    }

    /**
     * Adds an empty row at the end of the table.
     *
     * @return $this
     */
    public function addRow() {
        return $this->append(new Row($this->columnCount()));
    }

    /**
     * Inserts an empty row at the given index.
     *
     * @param $index
     * @return $this
     */
    public function insertRowAt($index) {
        return $this->insertAt($index, new Row($this->columnCount()));
    }

    /**
     * Adds an empty column at the end of the table
     *
     * @return $this
     */
    public function addColumn() {
        foreach ($this->childElements as $row) {
            /** @var $row Row */
            $row->append(new Cell());
        }
        return $this;
    }

    /**
     * Inserts an empty column at the given index.
     *
     * @param $index
     * @return $this
     */
    public function insertColumnAt($index) {
        foreach ($this->childElements as $row) {
            /** @var $row Row */
            $row->insertAt($index, new Cell());
        }
        return $this;
    }

    /**
     * Returns the cell at the given row and column.
     *
     * @param int $row
     * @param int $column
     * @return null|Cell
     */
    public function getCell($row = 1, $column = 1) {
        return $this->getChild($row)->getChild($column);
    }

    /**
     * Replaces the cell at given row and column.
     *
     * @param Cell $cell
     * @param int $row
     * @param int $column
     * @return $this
     * @throws \InvalidArgumentException
     */
    public function setCell(Cell $cell, $row = 1, $column = 1) {
        if (!($cell instanceof Cell))
            throw new \InvalidArgumentException("Argument cell must be an object of type Cell.");
        $this->getChild($row)->childElements[$column] = $cell;
        return $this;
    }

    /**
     * Appends some text with given style to a cell at the specified row and column.
     *
     * @param int $row
     * @param int $column
     * @param string $text
     * @param array $style
     * @return $this
     */
    public function addTextToCell($row = 1, $column = 1, $text = "", $style = array()) {
        $this->getCell($row, $column)->appendText($text, $style);
        return $this;
    }

    /**
     * Sets the width of a column.
     *
     * @param $column
     * @param $width
     * @return $this
     */
    public function setColumnWidth($column = 1, $width = 2032000) {
        foreach ($this->children() as $row) {
            /** @var $row Row */
            $cell = $row->getChild($column);
            /** @var $cell Cell */
            $cell->setWidth($width);
        }
        return $this;
    }

    /**
     * Sets the height of a row.
     *
     * @param $row
     * @param int $height
     */
    public function setRowHeight($row, $height = 370840) {
        /** @var $r Row */
        $r = $this->getChild($row);
        $r->setHeight($height);
    }

    /**
     * Read a Table from the given xml string.
     *
     * @param \QuatreCentQuatre\PHPPowerPoint\Readers\XMLReaderWithRelations $reader
     * @return mixed
     */
    public static function readFromXML(XMLReaderWithRelations $reader) {
        $table = new Table();
        $widths = array();

        $read = true;
        while ($read) {
            if ($reader->name == "a:gridCol") {
                $widths[] = intval($reader->getAttribute('w'));
            }
            if ($reader->name == "a:tr") {
                $table->append(Row::readFromXML($reader->subXML($reader->readOuterXml())));
                $reader->next();
                continue;
            }
            $read = $reader->read();
        }
        foreach ($widths as $i => $width) {
            $table->setColumnWidth($i, $width);
        }
        return $table;
    }

    /**
     * Writes an OpenXML to the XML writer.
     *
     * @param \XMLWriter $writer
     * @return mixed
     */
    function writeToXML(\XMLWriter $writer)
    {
        /** @var Row $firstRow */
        $firstRow = $this->getFirstChild();

        $writer->startElement('a:graphicData');
        $writer->writeAttribute('uri', 'http://schemas.openxmlformats.org/drawingml/2006/table');
        $writer->startElement('a:tbl');
        $writer->writeRaw('<a:tblPr firstRow="1" bandRow="1"><a:tableStyleId>{2D5ABB26-0587-4C30-8999-92F81FD0307C}</a:tableStyleId></a:tblPr>');
        $writer->startElement('a:tblGrid');
        foreach ($firstRow->children() as $cell) {
            /** @var Cell $cell */
            $writer->startElement('a:gridCol');
            $writer->writeAttribute('w', $cell->getWidth());
            $writer->endElement();
        }
        $writer->endElement();
        foreach ($this->childElements as $child) {
            /** @var Element $child */
            $child->writeToXML($writer);
        }
        $writer->endElement();
        $writer->endElement();
    }
}