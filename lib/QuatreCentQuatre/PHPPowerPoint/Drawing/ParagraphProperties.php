<?php


namespace QuatreCentQuatre\PHPPowerPoint\Drawing;


use QuatreCentQuatre\PHPPowerPoint\IOpenXMLElement;
use QuatreCentQuatre\PHPPowerPoint\Readers\XMLReaderWithRelations;

class ParagraphProperties implements IOpenXMLElement
{

    private $bold = null;
    private $italic = null;
    private $underline = null;
    private $size = null;
    private $caps = null;
    private $subscript = null;
    private $superscript = null;
    private $spacing = null;
    private $strikethrough = null;
    private $font = null;

    /** @var Color */
    private $color = null;

    /** @var Outline */
    private $outline = null;

    private $href = null;

    public function __clone()
    {
        if ($this->color != null)
            $this->color = clone $this->color;
    }

    /** @var TextRun */
    public $parent = null;

    /**
     * Sets the paragraph properties.
     *
     * @param array $style
     * @return $this
     */
    public function set($style = array())
    {
        if (isset($style["bold"]))
            $this->bold = $style["bold"] == true;
        if (isset($style["italic"]))
            $this->italic = $style["italic"] == true;
        if (isset($style["underline"]) && gettype($style["underline"]) === "string")
            $this->underline = $style["underline"];
        if (isset($style["size"]) && $style["size"] > 0)
            $this->size = $style["size"];
        if (isset($style["color"])) {
            if (gettype($style["color"]) === "string" && preg_match("/^[A-Z0-9]{6}$/", strtoupper($style["color"]))) {
                $this->color = Color::hex($style["color"]);
            }
            if (gettype($style["color"]) === "array") {
                $this->color = Color::rgb($style["color"][0], $style["color"][1], $style["color"][2]);
            }
        }
        if (isset($style["caps"]) && gettype($style["caps"]) === "string" && !(array_search($style["caps"], ['all', 'small', 'none']) === FALSE))
            $this->caps = $style["caps"];
        if (isset($style["subscript"])) {
            $this->subscript = $style["subscript"] == true;
            if ($this->subscript)
                $this->superscript = null;
        }
        if (isset($style["superscript"])) {
            $this->superscript = $style["superscript"] == true;
            if ($this->superscript)
                $this->subscript = null;
        }
        if (isset($style["spacing"]) && gettype($style["spacing"]) == "integer")
            $this->spacing = $style["spacing"];
        if (isset($style["strikethrough"]) && gettype($style["strikethrough"]) === "string" && !(array_search($style["strikethrough"], ['dblStrike', 'sngStrike', 'noStrike']) === FALSE))
            $this->strikethrough = $style["strikethrough"];
        if (isset($style["outline"]) && gettype($style["outline"]) === "array") {
            $this->outline = new Outline($style["outline"]);
        }
        if (isset($style["font"]) && gettype($style["font"]) === "string")
            $this->font = $style["font"];
        return $this;
    }

    /**
     * Returns paragraph visual properties in an array.
     *
     * @return array
     */
    public function get()
    {
        return array(
            'bold' => $this->bold,
            'font' => $this->font,
            'italic' => $this->italic,
            'underline' => $this->underline,
            'size' => $this->size,
            'color' => "" . $this->color,
            'caps' => $this->caps,
            'subscript' => $this->subscript,
            'superscript' => $this->superscript,
            'spacing' => $this->spacing,
            'strikethrough' => $this->strikethrough,
            'outline' => $this->outline->get()
        );
    }

    /**
     * Get/Sets the href for the text run. If href is equal to false, href will be unset.
     *
     * @param string|boolean $href
     * @return string|null
     */
    public function href($href = null)
    {
        if ($href === FALSE)
            $this->href = null;
        if (gettype($href) != "string")
            return $this->href;
        if ($href != null)
            $this->href = $href;
        return $this->href;
    }

    /**
     * Read ParagraphProperties from the given xml string.
     *
     * @param \QuatreCentQuatre\PHPPowerPoint\Readers\XMLReaderWithRelations $reader
     * @return ParagraphProperties
     */
    public static function readFromXML(XMLReaderWithRelations $reader)
    {
        $pPr = new ParagraphProperties();

        $reader->read();
        $pPr->set(array(
            'bold' => $reader->getAttribute("b"),
            'italic' => $reader->getAttribute("i"),
            'underline' => $reader->getAttribute("u"),
            'size' => intval($reader->getAttribute("sz")),
            'caps' => $reader->getAttribute("cap"),
            'spacing' => intval($reader->getAttribute("spc")),
            'strikethrough' => $reader->getAttribute("strike"),
            'subscript' => (intval($reader->getAttribute("baseline")) == -25000 ? true : null),
            'superscript' => (intval($reader->getAttribute("baseline")) == 30000 ? true : null)
        ));

        $read = true;
        while ($read) {
            if ($reader->name == "a:ln") {
                $pPr->outline = Outline::readFromXML($reader->subXML($reader->readOuterXml()));
                $reader->next();
                continue;
            }
            if ($reader->name == "a:solidFill") {
                $pPr->color = Color::readFromXML($reader->subXML($reader->readOuterXml()));
                $reader->next();
                continue;
            }
            if ($reader->name == "a:latin") {
                $pPr->font = $reader->getAttribute('typeface');
                $reader->next();
                continue;
            }
            if ($reader->name == "a:hlinkClick") {
                $rel = $reader->getRelation($reader->getAttribute('r:id'));
                if ($reader->getAttribute('action') == "ppaction://hlinksldjump") {
                    $pPr->href = "slide://$rel";
                } else {
                    $pPr->href = "$rel";
                }
            }
            $read = $reader->read();
        }
        return $pPr;
    }

    /**
     * Writes an OpenXML to the XML writer.
     *
     * @param \XMLWriter $writer
     * @return mixed
     */
    function writeToXML(\XMLWriter $writer)
    {
        $writer->startElement("a:rPr");
        if ($this->bold != null)
            $writer->writeAttribute('b', $this->bold);
        if ($this->italic != null)
            $writer->writeAttribute('i', $this->italic);
        if ($this->underline != null)
            $writer->writeAttribute('u', $this->underline);
        if ($this->size != null)
            $writer->writeAttribute('sz', $this->size);
        if ($this->spacing != null)
            $writer->writeAttribute('spc', $this->spacing);
        if ($this->caps != null)
            $writer->writeAttribute('cap', $this->caps);
        if ($this->strikethrough != null)
            $writer->writeAttribute('strike', $this->strikethrough);
        if ($this->superscript != null)
            $writer->writeAttribute('baseline', 30000);
        if ($this->subscript != null)
            $writer->writeAttribute('baseline', -25000);
        $writer->writeAttribute('lang', "en-US");
        $writer->writeAttribute('dirty', 0);
        $writer->writeAttribute('smtClean', 0);
        if ($this->outline != null)
            $this->outline->writeToXML($writer);
        if ($this->color != null) {
            $this->color->writeToXML($writer);
        }
        if ($this->font != null) {
            $writer->writeRaw('<a:latin typeface="' . $this->font . '"/>');
            $writer->writeRaw('<a:cs typeface="' . $this->font . '"/>');
        }
        if (!empty($this->href)) {
            $writer->startElement('a:hlinkClick');
            if (preg_match("#^(http)[s]?(://)#", $this->href)) {
                $id = $this->parent->getSlide()->addChildRelation($this->href, 'http://schemas.openxmlformats.org/officeDocument/2006/relationships/hyperlink', 'External');
                $writer->writeAttribute('r:id', $id);
            }
            if (preg_match("#^slide://#", $this->href)) {
                $href = substr($this->href, 8);
                $id = $this->parent->getSlide()->addChildRelation($href, 'http://schemas.openxmlformats.org/officeDocument/2006/relationships/slide');
                $writer->writeAttribute('r:id', $id);
                $writer->writeAttribute('action', "ppaction://hlinksldjump");
            }
            $writer->endElement();
        }
        $writer->endElement();
    }
}