<?php


namespace QuatreCentQuatre\PHPPowerPoint\Drawing;


use QuatreCentQuatre\PHPPowerPoint\Presentation\Element;
use QuatreCentQuatre\PHPPowerPoint\Readers\XMLReaderWithRelations;

class Row extends Element {

    /** @var int */
    private $height;

    public function __construct($cells = 0, $height = 370840) {
        for ($i = 0; $i < $cells; $i++) {
            $this->append(new Cell());
        }
    }

    /**
     * Returns wherever an Element is allowed as child or not.
     * Override this to edit allowed types.
     *
     * @param Element $e
     * @return bool
     */
    protected function isAllowed(Element $e) {
        $test = false;

        if ($e instanceof Cell)
            $test = true;

        return $test;
    }

    /**
     * Sets the height of the row in EMUs.
     *
     * @param int $height
     * @return $this
     * @throws \OutOfBoundsException
     */
    public function setHeight($height = 370840) {
        if ($height < -27273042329600 || $height > 27273042329600)
            throw new \OutOfBoundsException("Height must be between -27273042329600 EMUs and 27273042329600 EMUs.");
        $this->height = $height;
        return $this;
    }

    /**
     * Returns the height of the row in EMUs.
     *
     * @return int
     */
    public function getHeight() {
        return $this->height;
    }

    /**
     * Read a Row from the given xml string.
     *
     * @param \QuatreCentQuatre\PHPPowerPoint\Readers\XMLReaderWithRelations $reader
     * @return Row
     */
    public static function readFromXML(XMLReaderWithRelations $reader) {
        $row = new Row();

        $read = true;
        while ($read) {
            if ($reader->name == "a:tc") {
                $row->append(Cell::readFromXML($reader->subXML($reader->readOuterXml())));
                $reader->next();
                continue;
            }
            if ($reader->name == "a:tr") {
                $row->setHeight(intval($reader->getAttribute('h')));
            }
            $read = $reader->read();
        }
        return $row;
    }

    /**
     * Writes an OpenXML to the XML writer.
     *
     * @param \XMLWriter $writer
     * @return mixed
     */
    function writeToXML(\XMLWriter $writer)
    {
        $writer->startElement('a:tr');
        $writer->writeAttribute('h', $this->getHeight());
        foreach ($this->childElements as $child) {
            /** @var Element $child */
            $child->writeToXML($writer);
        }
        $writer->endElement();
    }
}