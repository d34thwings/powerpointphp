<?php


namespace QuatreCentQuatre\PHPPowerPoint\Drawing;

use QuatreCentQuatre\PHPPowerPoint\IOpenXMLElement;
use QuatreCentQuatre\PHPPowerPoint\Readers\XMLReaderWithRelations;

/**
 * Class that contains the position of a shape.
 *
 * @package QuatreCentQuatre\PHPPowerPoint\Drawing
 */
final class Offset extends Point2D implements IOpenXMLElement {

    protected $MIN_X = -27273042329600;
    protected $MAX_X = 27273042329600;
    protected $MIN_Y = -27273042329600;
    protected $MAX_Y = 27273042329600;

    /**
     * Read an Offset from the given xml string.
     *
     * @param XMLReaderWithRelations $reader
     * @return Offset
     */
    public static function readFromXML(XMLReaderWithRelations $reader) {
        $offset = new Offset();
        $reader->read();
        $offset->set(intval($reader->getAttribute("x")), intval($reader->getAttribute("y")));
        return $offset;
    }

    /**
     * Writes an OpenXML to the XML writer.
     *
     * @param \XMLWriter $writer
     * @return mixed
     */
    function writeToXML(\XMLWriter $writer)
    {
        $writer->startElement("a:off");
        $writer->writeAttribute('x', $this->x);
        $writer->writeAttribute('y', $this->y);
        $writer->endElement();
    }
}