<?php


namespace QuatreCentQuatre\PHPPowerPoint\Drawing;

use QuatreCentQuatre\PHPPowerPoint\IOpenXMLElement;
use QuatreCentQuatre\PHPPowerPoint\Readers\XMLReaderWithRelations;

/**
 * Class that contains the size of a shape.
 *
 * @package QuatreCentQuatre\PHPPowerPoint\Drawing
 */
final class Extents extends Point2D implements IOpenXMLElement {

    protected $MIN_X = 1;
    protected $MAX_X = 27273042316900;
    protected $MIN_Y = 1;
    protected $MAX_Y = 27273042316900;

    /**
     * Read an Extents from the given xml string.
     *
     * @param XMLReaderWithRelations $reader
     * @return Extents
     */
    public static function readFromXML(XMLReaderWithRelations $reader) {
        $extents = new Extents();
        $reader->read();
        $extents->set(intval($reader->getAttribute("cx")), intval($reader->getAttribute("cy")));
        return $extents;
    }

    /**
     * Writes an OpenXML to the XML writer.
     *
     * @param \XMLWriter $writer
     * @return mixed
     */
    function writeToXML(\XMLWriter $writer)
    {
        $writer->startElement("a:ext");
        $writer->writeAttribute('cx', $this->x);
        $writer->writeAttribute('cy', $this->y);
        $writer->endElement();
    }
}