<?php


namespace QuatreCentQuatre\PHPPowerPoint\Drawing;


class PixelInchConverter {

    const EMU_PER_INCH =  914400;

    /**
     * Converts EMUs to inches.
     *
     * @param int $value
     * @return float
     */
    public final static function emusToInches($value) {
        return $value / self::EMU_PER_INCH;
    }

    /**
     * Converts EMUs to pixels according to the given DPI.
     *
     * @param int $value
     * @param int $dpi
     * @return float
     */
    public final static function emusToPixels($value, $dpi) {
        return self::emusToInches($value) * $dpi;
    }

    /**
     * Converts inches to EMUs.
     *
     * @param float $value
     * @return int
     */
    public final static function inchesToEMUs($value) {
        return round($value * self::EMU_PER_INCH);
    }

    /**
     * Converts pixels to EMUs according to the given DPI.
     *
     * @param $value
     * @param $dpi
     * @return float
     */
    public final static function pixelsToEMUs($value, $dpi) {
        return round(self::inchesToEMUs($value) / $dpi);
    }
} 