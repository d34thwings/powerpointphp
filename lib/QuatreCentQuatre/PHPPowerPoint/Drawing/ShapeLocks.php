<?php


namespace QuatreCentQuatre\PHPPowerPoint\Drawing;


use QuatreCentQuatre\PHPPowerPoint\IOpenXMLElement;
use QuatreCentQuatre\PHPPowerPoint\Readers\XMLReaderWithRelations;

final class ShapeLocks implements IOpenXMLElement
{

    private $noSelect = false;
    private $noTextEdit = false;
    private $noMove = false;
    private $noResize = false;
    private $noRot = false;
    private $noGrp = false;

    /**
     * Constructor.
     *
     * @param array $locks
     */
    public function __construct($locks = array())
    {
        $this->set($locks);
    }

    /**
     * Sets all the locks for the parent shape.
     *
     * @param array $locks
     * @return $this
     */
    public function set($locks)
    {
        if (gettype($locks) != "array") return $this;

        if (isset($locks['noSelect']))
            $this->noSelect = $locks['noSelect'];
        if (isset($locks['noTextEdit']))
            $this->noTextEdit = $locks['noTextEdit'];
        if (isset($locks['noMove']))
            $this->noMove = $locks['noMove'];
        if (isset($locks['noResize']))
            $this->noResize = $locks['noResize'];
        if (isset($locks['noRot']))
            $this->noRot = $locks['noRot'];
        if (isset($locks['noGrp']))
            $this->noGrp = $locks['noGrp'];
        return $this;
    }

    /**
     * Read a ShapeLocks from the given xml string.
     *
     * @param \QuatreCentQuatre\PHPPowerPoint\Readers\XMLReaderWithRelations $reader
     * @return mixed
     */
    static function readFromXML(XMLReaderWithRelations $reader)
    {
        $spLocks = new ShapeLocks();

        $reader->read();
        $spLocks->noSelect = $reader->getAttribute('noSelect') == true;
        $spLocks->noTextEdit = $reader->getAttribute('noTextEdit') == true;
        $spLocks->noMove = $reader->getAttribute('noMove') == true;
        $spLocks->noResize = $reader->getAttribute('noResize') == true;
        $spLocks->noRot = $reader->getAttribute('noRot') == true;
        $spLocks->noGrp = $reader->getAttribute('noGrp') == true;

        return $spLocks;
    }

    /**
     * Writes a ShapeLocks to the XML writer.
     *
     * @param \XMLWriter $writer
     * @return mixed
     */
    function writeToXML(\XMLWriter $writer)
    {
        $writer->startElement('a:spLocks');

        if ($this->noSelect)
            $writer->writeAttribute('noSelect', $this->noSelect);
        if ($this->noTextEdit)
            $writer->writeAttribute('noTextEdit', $this->noTextEdit);
        if ($this->noMove)
            $writer->writeAttribute('noMove', $this->noMove);
        if ($this->noResize)
            $writer->writeAttribute('noResize', $this->noResize);
        if ($this->noRot)
            $writer->writeAttribute('noRot', $this->noRot);
        if ($this->noGrp)
            $writer->writeAttribute('noGrp', $this->noGrp);

        $writer->endElement();
    }
}