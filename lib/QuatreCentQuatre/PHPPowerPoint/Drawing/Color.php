<?php


namespace QuatreCentQuatre\PHPPowerPoint\Drawing;


use QuatreCentQuatre\PHPPowerPoint\IOpenXMLElement;
use QuatreCentQuatre\PHPPowerPoint\Readers\XMLReaderWithRelations;

final class Color implements IOpenXMLElement {

    private $R = 0;
    private $G = 0;
    private $B = 0;

    /**
     * Color object can only be created by hex() and rgb() static functions.
     *
     * @param int $R
     * @param int $G
     * @param int $B
     */
    private function __construct($R, $G, $B) {
        $this->R = $R;
        $this->G = $G;
        $this->B = $B;
    }

    /**
     * Returns color as an array with RGB colors.
     *
     * @return array
     */
    public function toRGB() {
        return array(
            'R' => $this->R,
            'G' => $this->G,
            'B' => $this->B,
        );
    }

    /**
     * Creates color from hex code.
     *
     * @param string $hex
     * @return null|Color
     */
    public static function hex($hex) {
        if (!preg_match("/^[A-F0-9]{6}$/", strtoupper($hex)))
            return null;

        if (substr($hex,0,1) == "#")
            $hex = substr($hex,1);

        $R = hexdec(substr($hex,0,2));
        $G = hexdec(substr($hex,2,2));
        $B = hexdec(substr($hex,4,2));

        return new Color($R, $G, $B);
    }

    /**
     * Creates color from RGB.
     *
     * @param int $R
     * @param int $G
     * @param int $B
     * @return null|Color
     */
    public static function rgb($R, $G, $B) {
        if ($R > 255 || $R < 0 || $G > 255 || $G < 0 || $B > 255 || $B < 0)
            return null;
        return new Color($R, $G, $B);
    }

    /**
     * Returns the color as hex code.
     *
     * @return string
     */
    public function __toString() {
        $R = dechex($this->R);
        If (strlen($R) < 2)
            $R = '0' . $R;

        $G = dechex($this->G);
        If (strlen($G) < 2)
            $G = '0' . $G;

        $B = dechex($this->B);
        If (strlen($B) < 2)
            $B = '0' . $B;
        return strtoupper($R . $G . $B);
    }


    /**
     * Read a Color from the given xml string.
     *
     * @param \QuatreCentQuatre\PHPPowerPoint\Readers\XMLReaderWithRelations $reader
     * @return Color|null
     */
    static function readFromXML(XMLReaderWithRelations $reader)
    {
        $color = null;

        $read = true;
        while ($read) {
            if ($reader->name == "a:srgbClr") {
                $color = Color::hex($reader->getAttribute('val'));
            }

            $read = $reader->read();
        }
        return $color;
    }

    /**
     * Writes a Color to the XML writer.
     *
     * @param \XMLWriter $writer
     * @return mixed
     */
    function writeToXML(\XMLWriter $writer)
    {
        $writer->writeRaw('<a:solidFill><a:srgbClr val="' . $this . '"/></a:solidFill>');
    }
}