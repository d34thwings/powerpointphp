<?php


namespace QuatreCentQuatre\PHPPowerPoint\Drawing;


use QuatreCentQuatre\PHPPowerPoint\IOpenXMLElement;
use QuatreCentQuatre\PHPPowerPoint\Readers\XMLReaderWithRelations;

final class Outline implements IOpenXMLElement {

    /** @var Color */
    private $color;

    private $weight;
    private $dashed;
    private $compound;

    private $dashPresets = [
        'solid',
        'dot',
        'sysDot',
        'dash',
        'sysDash',
        'lgDash',
        'dashDot',
        'sysDashDot',
        'lgDashDot',
        'lgDashDotDot',
        'sysDashDotDot'
    ];
    private $compounds = [
        'dbl',
        'sng',
        'thickThin',
        'thinThick',
        'tri'
    ];

    public function __construct($style = array())
    {
        $this->set($style);
    }

    /**
     * Sets the style of the outline.
     *
     * @param array $style
     * @return $this
     */
    public function set($style)
    {
        if (gettype($style) != "array") return $this;

        if (isset($style["dashed"]) && gettype($style["dashed"]) === "string")
            if (!(array_search($style["compound"], $this->dashPresets) === FALSE))
                $this->dashed = $style["dashed"];
        if (isset($style["compound"]) && gettype($style["compound"]) === "string")
            if (!(array_search($style["compound"], $this->compounds) === FALSE))
                $this->compound = $style["compound"];
        if (isset($style["weight"]) && $style["weight"] > 0)
            $this->weight = $style["weight"];
        if (isset($style["color"])) {
            if (gettype($style["color"]) === "string" && preg_match("/^[A-Z0-9]{6}$/", strtoupper($style["color"]))) {
                $this->color = Color::hex($style["color"]);
            }
            if (gettype($style["color"]) === "array") {
                $this->color = Color::rgb($style["color"][0], $style["color"][1], $style["color"][2]);
            }
        }
        return $this;
    }

    /**
     * Returns the style of the outline.
     *
     * @return array
     */
    public function get()
    {
        return array(
            "weight"    => $this->weight,
            "dashed"    => $this->dashed,
            "compound"    => $this->compound,
            "color"     => "" . $this->color
        );
    }

    /**
     * Read an OPenXML element from the given xml string.
     *
     * @param \QuatreCentQuatre\PHPPowerPoint\Readers\XMLReaderWithRelations $reader
     * @return mixed
     */
    static function readFromXML(XMLReaderWithRelations $reader)
    {
        $ln = new Outline();

        $reader->read();
        $ln->set(array(
            'weight'          => $reader->getAttribute("w"),
            'compound'        => $reader->getAttribute("cmpd")
        ));

        $read = true;
        while ($read) {
            if ($reader->name == "a:solidFill") {
                $ln->color = Color::readFromXML($reader->subXML($reader->readOuterXml()));
                $reader->next();
                continue;
            }
            if ($reader->name == "a:prstDash") {
                $ln->dashed = $reader->getAttribute('val');
            }
            $read = $reader->read();
        }
        return $ln;
    }

    /**
     * Writes an OpenXML to the XML writer.
     *
     * @param \XMLWriter $writer
     * @return mixed
     */
    function writeToXML(\XMLWriter $writer)
    {
        $writer->startElement('a:ln');
        if ($this->weight != null)
            $writer->writeAttribute('w', $this->weight);
        if ($this->compound != null)
            $writer->writeAttribute('cmpd', $this->compound);
        if ($this->color != null)
            $this->color->writeToXML($writer);
        if ($this->dashed != null) {
            $writer->startElement("a:prstDash");
            $writer->writeAttribute('val', $this->dashed);
            $writer->endElement();
        }
        $writer->endElement();
    }
}