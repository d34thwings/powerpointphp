<?php


namespace QuatreCentQuatre\PHPPowerPoint\Drawing;

/**
 * Coordinates class. Override the constants to have different bounds for each axis.
 *
 * @package QuatreCentQuatre\PHPPowerPoint\Drawing
 */
abstract class Point2D {

    protected $MIN_X = 0;
    protected $MAX_X = 0;
    protected $MIN_Y = 0;
    protected $MAX_Y = 0;

    protected $x;
    protected $y;

    /**
     * Constructor. The default values for x and y are their minimum value.
     *
     * @param int $x
     * @param int $y
     */
    public function __construct($x = null, $y = null) {
        $this->set($x, $y);
    }

    /**
     * Returns the x and y values.
     *
     * @return array
     */
    public final function get() {
        return ["x" => $this->x, "y" => $this->y];
    }

    /**
     * Returns the x value;
     *
     * @return int
     */
    public final function getX() { return $this->$x; }

    /**
     * Returns the y value.
     *
     * @return int
     */
    public final function getY() { return $this->$y; }

    /**
     * Sets the x and y value.
     *
     * @param int $x
     * @param int $y
     * @return $this
     */
    public final function set($x, $y) {
        $this->setX($x);
        $this->setY($y);
        return $this;
    }

    /**
     * Sets the x value.
     *
     * @param int $x
     * @return $this
     */
    public final function setX($x) {
        if ($x == null)
            $x = $this->MIN_X;
        if ($x >= $this->MIN_X && $x <= $this->MAX_X)
            $this->x = $x;
        return $this;
    }

    /**
     * Sets the y value.
     *
     * @param int $y
     * @return $this
     */
    public final function setY($y) {
        if ($y == null)
            $y = $this->MIN_Y;
        if ($y >= $this->MIN_Y && $y <= $this->MAX_Y)
            $this->y = $y;
        return $this;
    }
} 