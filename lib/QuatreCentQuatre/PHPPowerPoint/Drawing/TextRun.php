<?php


namespace QuatreCentQuatre\PHPPowerPoint\Drawing;


use QuatreCentQuatre\PHPPowerPoint\Presentation\Element;
use QuatreCentQuatre\PHPPowerPoint\Readers\XMLReaderWithRelations;

class TextRun extends Element
{

    private $text = "";

    /** @var ParagraphProperties */
    private $pPr = null;

    /**
     * Returns wherever an Element is allowed as child or not.
     * Override this to edit allowed types.
     *
     * @param Element $e
     * @return bool
     */
    protected function isAllowed(Element $e)
    {
        return false;
    }

    /**
     * Constructor.
     *
     * @param string $text
     * @param array $style
     */
    public function __construct($text = "", $style = array())
    {
        parent::__construct();
        $this->text = $text;
        $this->pPr = new ParagraphProperties();
        $this->pPr->parent = $this;
        $this->setStyle($style);
    }

    /**
     * Sets a link to a website on the text.
     *
     * @param string $href
     * @return $this
     * @throws \InvalidArgumentException
     */
    public function setHyperLinkToWebsite($href)
    {
        if (gettype($href) != "string")
            throw new \InvalidArgumentException("Argument must be a string.");
        if (!preg_match("#^http[s]?://#", $href))
            return $this;
        $this->pPr->href($href);
        return $this;
    }

    /**
     * Sets a link to a slide on the text.
     *
     * @param int $index
     * @return $this
     * @throws \InvalidArgumentException
     */
    public function setHyperLinkToSlide($index)
    {
        if (gettype($index) != "integer")
            throw new \InvalidArgumentException("Argument must be an integer.");

        $this->pPr->href("slide://slide$index.xml");
        return $this;
    }

    /**
     * Removes the HyperLink from the text.
     *
     * @return $this
     */
    public function removeHyperLink()
    {
        $this->pPr->href(FALSE);
        return $this;
    }

    /**
     * Returns the target of the HyperLink.
     *
     * @return string
     */
    public function getHyperLink()
    {
        return $this->pPr->href();
    }

    /**
     * Sets the font style.
     *
     * @param array $style
     * @return $this
     */
    public function setStyle($style = array())
    {
        $this->pPr->set($style);
        return $this;
    }

    /**
     * Read a TextRun from the given xml string.
     *
     * @param \QuatreCentQuatre\PHPPowerPoint\Readers\XMLReaderWithRelations $reader
     * @return Paragraph
     */
    public static function readFromXML(XMLReaderWithRelations $reader)
    {
        $r = new TextRun();

        $read = true;
        while ($read) {
            if ($reader->name == "a:rPr") {
                $r->pPr = ParagraphProperties::readFromXML($reader->subXML($reader->readOuterXml()));
                $r->pPr->parent = $r;
                $reader->next();
                continue;
            }
            if ($reader->name == "a:t") {
                $r->text = $reader->readString();
                $reader->next();
                continue;
            }

            $read = $reader->read();
        }
        return $r;
    }

    /**
     * Writes an OpenXML to the XML writer.
     *
     * @param \XMLWriter $writer
     * @return mixed
     */
    function writeToXML(\XMLWriter $writer)
    {
        $writer->startElement("a:r");
        $this->pPr->writeToXML($writer);
        $writer->writeElement('a:t', $this->text);
        $writer->endElement();
    }
}