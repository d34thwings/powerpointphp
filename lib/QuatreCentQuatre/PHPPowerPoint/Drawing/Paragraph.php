<?php


namespace QuatreCentQuatre\PHPPowerPoint\Drawing;


use QuatreCentQuatre\PHPPowerPoint\Presentation\Element;
use QuatreCentQuatre\PHPPowerPoint\Readers\XMLReaderWithRelations;

final class Paragraph extends Element
{

    private $allowListStyle;
    private $align;

    /**
     * Returns wherever an Element is allowed as child or not.
     * Override this to edit allowed types.
     *
     * @param Element $e
     * @return bool
     */
    protected function isAllowed(Element $e)
    {
        $test = false;

        if ($e instanceof TextRun)
            $test = true;

        return $test;
    }

    /**
     * Constructor.
     *
     * @param string $text
     * @param $style
     */
    public function __construct($text = "", $style = array())
    {
        parent::__construct();
        $this->allowListStyle = true;
        if ($text != "")
            $this->appendText($text, $style);
    }

    /**
     * Sets if this paragraph will apply the style of the text box.
     *
     * @param boolean $bool
     * @throws \InvalidArgumentException
     */
    public function allowListStyle($bool)
    {
        if (gettype($bool) != "boolean")
            throw new \InvalidArgumentException("Argument must be a boolean.");

        $this->allowListStyle = $bool;
    }

    /**
     * Sets a link to a website on the paragraph.
     *
     * @param string $href
     * @return $this
     * @throws \InvalidArgumentException
     */
    public function setHyperLinkToWebsite($href)
    {
        foreach ($this->children() as $r) {
            /** @var $r TextRun */
            $r->setHyperLinkToWebsite($href);
        }
        return $this;
    }

    /**
     * Sets a link to a slide on the paragraph.
     *
     * @param int $index
     * @return $this
     * @throws \InvalidArgumentException
     */
    public function setHyperLinkToSlide($index)
    {
        foreach ($this->children() as $r) {
            /** @var $r TextRun */
            $r->setHyperLinkToSlide($index);
        }
        return $this;
    }

    /**
     * Removes HyperLink on all child elements.
     *
     * @return $this
     */
    public function removeHyperLink()
    {
        foreach ($this->children() as $r) {
            /** @var $r TextRun */
            $r->removeHyperLink();
        }
        return $this;
    }

    /**
     * appends some text to the paragraph.
     *
     * @param string $text
     * @param array $style
     * @return $this
     * @throws \InvalidArgumentException
     */
    public function appendText($text, $style = array())
    {
        if (gettype($text) != "string")
            throw new \InvalidArgumentException("Argument text must be a string.");
        if (gettype($style) != "array")
            throw new \InvalidArgumentException("Argument style must be an array.");

        return $this->append(new TextRun($text, $style));
    }

    /**
     * Sets the paragraph style (color, font size, alignment, etc...).
     *
     * @param array $style
     */
    public function setStyle($style)
    {
        if (isset($style['align']) && array_search($style['align'], array('l', 'r', 'ctr', 'just')))
            $this->align = $style['align'];
        foreach ($this->childElements as $child) {
            if ($child instanceof TextRun) {
                $child->setStyle($style);
            }
        }
    }

    /**
     * Returns the text alignment.
     *
     * @return string|null
     */
    public function getAlignment()
    {
        return $this->align;
    }

    /**
     * Read a TextRun from the given xml string.
     *
     * @param \QuatreCentQuatre\PHPPowerPoint\Readers\XMLReaderWithRelations $reader
     * @return Paragraph
     */
    public static function readFromXML(XMLReaderWithRelations $reader)
    {
        $p = new Paragraph();

        $read = true;
        while ($read) {
            if ($reader->name == "a:buNone") {
                $p->allowListStyle(false);
            }
            if ($reader->name == "a:r") {
                $p->append(TextRun::readFromXML($reader->subXML($reader->readOuterXml())));
                $reader->next();
                continue;
            }
            if ($reader->name == "a:pPr") {
                $p->setStyle(array('align' => $reader->getAttribute('algn')));
            }
            $read = $reader->read();
        }
        return $p;
    }

    /**
     * Writes an OpenXML to the XML writer.
     *
     * @param \XMLWriter $writer
     * @return mixed
     */
    function writeToXML(\XMLWriter $writer)
    {
        $writer->startElement("a:p");
        if ($this->align != null)
            $writer->writeRaw('<a:pPr algn="' . $this->align . '"/>');
        foreach ($this->childElements as $child) {
            /** @var $child Element */
            $child->writeToXML($writer);
        }
        $writer->endElement();
    }
}