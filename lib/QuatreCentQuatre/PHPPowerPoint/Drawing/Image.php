<?php


namespace QuatreCentQuatre\PHPPowerPoint\Drawing;


use QuatreCentQuatre\PHPPowerPoint\Exceptions\PHPPowerPointException;
use QuatreCentQuatre\PHPPowerPoint\Presentation\Element;
use QuatreCentQuatre\PHPPowerPoint\Readers\Exceptions\InvalidFileException;
use QuatreCentQuatre\PHPPowerPoint\Readers\XMLReaderWithRelations;

class Image extends Element {

    protected $image;
    protected $format;

    /**
     * Returns wherever an Element is allowed as child or not.
     * Override this to edit allowed types.
     *
     * @param Element $e
     * @return bool
     */
    protected function isAllowed(Element $e)
    {
        return false;
    }

    /**
     * Loads the image from binary data (string).
     *
     * @param string $image
     * @param string $format
     * @throws \QuatreCentQuatre\PHPPowerPoint\Exceptions\PHPPowerPointException
     * @throws \InvalidArgumentException
     */
    public function setImageFromBinary($image, $format) {
        if (gettype($image) != "string")
            throw new \InvalidArgumentException("Argument image must be of type string.");
        if (gettype($format) != "string")
            throw new \InvalidArgumentException("Argument format must be of type string.");
        if (array_search($format, ["jpg", "png", "gif", "bmp", "jpeg"]) === FALSE)
            throw new PHPPowerPointException("Image format $format is not supported.");

        $this->image = $image;
        $this->format = $format;
    }

    /**
     * Loads the image from path.
     *
     * @param string $path
     * @return $this
     * @throws \QuatreCentQuatre\PHPPowerPoint\Exceptions\PHPPowerPointException
     * @throws \QuatreCentQuatre\PHPPowerPoint\Readers\Exceptions\InvalidFileException
     * @throws \InvalidArgumentException
     */
    public function setImageFromPath($path) {
        if (gettype($path) != "string")
            throw new \InvalidArgumentException("Argument must be of type string.");
        if (!file_exists($path))
            throw new InvalidFileException("File $path doesn't exist.");

        $format = pathinfo($path, PATHINFO_EXTENSION);
        if (!array_search($format, ["jpg", "png", "gif", "bmp"]))
            throw new PHPPowerPointException("Image format $format is not supported.");

        $handle = fopen($path, 'rb');
        $image = fread($handle, filesize($path));
        fclose($handle);

        $this->image = $image;
        $this->format = $format;
        return $this;
    }

    /**
     * Read an Image from the given xml string.
     *
     * @param \QuatreCentQuatre\PHPPowerPoint\Readers\XMLReaderWithRelations $reader
     * @return mixed
     */
    static function readFromXML(XMLReaderWithRelations $reader)
    {
        $pic = new Image();

        $read = true;
        while ($read) {
            if ($reader->name == "a:blip") {
                $pic->setImageFromBinary(
                    $reader->getImageFromRelation($reader->getAttribute("r:embed")),
                    $reader->getImageFormatFromRelation($reader->getAttribute("r:embed"))
                );
                $reader->next();
                continue;
            }

            $read = $reader->read();
        }

        return $pic;
    }

    /**
     * Writes an Image to the XML writer.
     *
     * @param \XMLWriter $writer
     * @return mixed
     */
    public function writeToXML(\XMLWriter $writer)
    {
        $writer->startElement('p:blipFill');
        $writer->startElement('a:blip');
        $id = $this->getSlide()->addImageRelation($this->image, $this->format);
        $writer->writeAttribute('r:embed', $id);
        $writer->endElement();
        $writer->writeRaw('<a:stretch><a:fillRect/></a:stretch>');
        $writer->endElement();
    }

    /**
     * Writes an Image to the XML writer with the Drawing namespace.
     *
     * @param \XMLWriter $writer
     */
    public function writeToXMLWithDrawingNS(\XMLWriter $writer)
    {
        $writer->startElement('a:blipFill');
        $writer->startElement('a:blip');
        $id = $this->getSlide()->addImageRelation($this->image, $this->format);
        $writer->writeAttribute('r:embed', $id);
        $writer->endElement();
        $writer->writeRaw('<a:stretch><a:fillRect/></a:stretch>');
        $writer->endElement();
    }
}