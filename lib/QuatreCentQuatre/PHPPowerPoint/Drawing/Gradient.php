<?php


namespace QuatreCentQuatre\PHPPowerPoint\Drawing;


use QuatreCentQuatre\PHPPowerPoint\Exceptions\PHPPowerPointException;
use QuatreCentQuatre\PHPPowerPoint\IOpenXMLElement;
use QuatreCentQuatre\PHPPowerPoint\Readers\XMLReaderWithRelations;

final class Gradient implements IOpenXMLElement
{

    private $colorStops = [];
    private $style = [];

    public function __construct($stops = array(), $style = array())
    {
        if (gettype($stops) != "array")
            throw new \InvalidArgumentException("Argument stop must be an array.");

        foreach ($stops as $stop) {
            if (gettype($stops) != "array")
                continue;

            if (isset($stop[0]) && isset($stop[1]))
                $this->addColorStop($stop[0], $stop[1]);
        }

        $this->setStyle($style);

        // TODO : Support radial gradients
    }

    public function setStyle($style)
    {
        if (!isset($style["type"]))
            $style["type"] = "linear";
        else
            $this->style["type"] = $style["type"];

        if (isset($style["angle"]))
            $this->style["angle"] = $style["angle"];
    }

    /**
     * Adds a color stop to the gradient.
     *
     * @param Color|string $color
     * @param int|string $position
     * @return $this
     */
    public function addColorStop($color, $position)
    {
        if (gettype($color) == "string")
            $color = Color::hex($color);
        if (gettype($position) == "string" && preg_match("/^[\d]{1,3}[%]$/", $position))
            $position = intval(substr($position, 0, strlen($position) - 1)) * 1000;
        if ($position < 0 && $position > 100000)
            return $this;

        $this->colorStops[] = [$color, $position];
        return $this;
    }

    /**
     * Reads a Gradient from the given xml string.
     *
     * @param \QuatreCentQuatre\PHPPowerPoint\Readers\XMLReaderWithRelations $reader
     * @return mixed
     */
    static function readFromXML(XMLReaderWithRelations $reader)
    {
        $gr = new Gradient();

        $read = true;
        while ($read) {
            if ($reader->name == "a:gs") {
                $gs = self::readColorStopFromXML($reader->subXML($reader->readOuterXml()));
                $gr->addColorStop($gs[0], $gs[1]);
                $reader->next();
                continue;
            }
            if ($reader->name == "a:lin") {
                $gr->setStyle([
                    "type" => "linear",
                    "angle" => $reader->getAttribute('ang')
                ]);
            }
            $read = $reader->read();
        }

        return $gr;
    }

    /**
     * Reads a Color stop from the given XML string.
     *
     * @param XMLReaderWithRelations $reader
     * @return array
     */
    private static function readColorStopFromXML(XMLReaderWithRelations $reader)
    {
        $stop = ["000000", 0];

        $reader->read();
        $stop[1] = $reader->getAttribute('pos');

        $read = true;
        while ($read) {
            if ($reader->name == "a:srgbClr") {
                $stop[0] = $reader->getAttribute('val');
                $reader->next();
                continue;
            }
            $read = $reader->read();
        }

        return $stop;
    }

    /**
     * Writes a Gradient to the XML writer.
     *
     * @param \XMLWriter $writer
     * @return mixed
     */
    function writeToXML(\XMLWriter $writer)
    {
        $writer->startElement('a:gradFill');
        $writer->writeAttribute('flip', 'none');
        $writer->writeAttribute('rotWithShape', true);
        $writer->startElement('a:gsLst');
        foreach ($this->colorStops as $stop) {
            $writer->startElement('a:gs');
            $writer->writeAttribute('pos', $stop[1]);
            $writer->startElement('a:srgbClr');
            $writer->writeAttribute('val', $stop[0] . "");
            $writer->endElement();
            $writer->endElement();
        }
        $writer->endElement();
        if ($this->style["type"] == "linear") {
            $writer->startElement('a:lin');
            if (!empty($this->style["angle"]))
                $writer->writeAttribute('ang', $this->style["angle"]);
            $writer->writeAttribute('scaled', false);
            $writer->endElement();
        }
        $writer->endElement();
    }
}