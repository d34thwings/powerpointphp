<?php


namespace QuatreCentQuatre\PHPPowerPoint;


use QuatreCentQuatre\PHPPowerPoint\Readers\XMLReaderWithRelations;

Interface IOpenXMLElement {

    const XML_NS_A = "http://schemas.openxmlformats.org/drawingml/2006/main";
    const XML_NS_P = "http://schemas.openxmlformats.org/presentationml/2006/main";
    const XML_NS_R = "http://schemas.openxmlformats.org/officeDocument/2006/relationships";

    /**
     * Read an OPenXML element from the given xml string.
     *
     * @param \QuatreCentQuatre\PHPPowerPoint\Readers\XMLReaderWithRelations $reader
     * @return mixed
     */
    static function readFromXML(XMLReaderWithRelations $reader);

    /**
     * Writes an OpenXML to the XML writer.
     *
     * @param \XMLWriter $writer
     * @return mixed
     */
    function writeToXML(\XMLWriter $writer);
} 