## PowerpointPHP

Librairie PowerPoint pour PHP permettant lecture et écriture dans le format OpenXML de fichiers PowerPoint. Supporte les thèmes, tableaux, styles de texte, images, vidéos, formes, transitions et animations basiques. Cette librairie propose aussi quelques fonctions utilitaires tel que la fusion de PowerPoint ou l'extraction de certaines diaposives pour créer un nouveau PowerPoint.

#Documentation

A venir.

#Auteur

Benjamin Delamarre
d34thwings@gmail.com