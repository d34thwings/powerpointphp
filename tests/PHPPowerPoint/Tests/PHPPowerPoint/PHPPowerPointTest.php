<?php


namespace PHPPowerPoint\Tests\PHPPowerPoint;


use Aspose\Cloud\Common\AsposeApp;
use Aspose\Cloud\Common\Utils;
use PHPPowerPoint\Tests\PHPPowerPointTestCase;
use QuatreCentQuatre\PHPPowerPoint\PHPPowerPoint;
use QuatreCentQuatre\PHPPowerPoint\Presentation\Builder\PresentationBuilder;
use QuatreCentQuatre\PHPPowerPoint\Presentation\Presentation;
use QuatreCentQuatre\PHPPowerPoint\Presentation\Slide\Slide;
use QuatreCentQuatre\PHPPowerPoint\Presentation\SlideLayout\SlideLayout;
use QuatreCentQuatre\PHPPowerPoint\Presentation\SlideMaster\SlideMaster;

final class PHPPowerPointTest extends PHPPowerPointTestCase {

    /** @var PresentationBuilder */
    private $presentationBuilder;

    /** @var SlideMaster */
    private $slideMaster;

    protected function setUp() {
        $this->presentationBuilder = new PresentationBuilder();
        $this->slideMaster = new SlideMaster("Test Master");
        $this->slideMaster->addSlideLayout(new SlideLayout("Test Layout"));
    }

    public function testGetSetPresentation() {
        $ppt = new PHPPowerPoint();
        $pres = new Presentation();

        $ppt->setPresentation($pres);

        $this->assertEquals($ppt->getPresentation(), $pres);
    }

    public function testSlice() {
        $ppt1 = new PHPPowerPoint();
        $ppt2 = new PHPPowerPoint();

        $slide1 = new Slide();
        $slide2 = new Slide();
        $ppt1->getPresentation()->addSlide($slide1)->addSlide($slide2);
        $ppt2->getPresentation()->addSlide($slide1);

        $this->assertEquals($ppt1->slice(array(0)), $ppt2);
    }

    public function testInsertSlides() {
        $ppt1 = new PHPPowerPoint();
        $ppt2 = new PHPPowerPoint();
        $ppt3 = new PHPPowerPoint();

        $slide1 = new Slide();
        $slide2 = new Slide();
        $ppt1->setPresentation($this->presentationBuilder->create()
            ->withSlideMaster($this->slideMaster)
            ->withSlide($slide1)
            ->now());
        $ppt2->setPresentation($this->presentationBuilder->create()
            ->withSlideMaster($this->slideMaster)
            ->withSlide($slide1)
            ->withSlide($slide2)
            ->now());
        $ppt3->setPresentation($this->presentationBuilder->create()
            ->withSlideMaster($this->slideMaster)
            ->withSlide($slide1)
            ->withSlide($slide2)
            ->now());

        $this->assertEquals($ppt1->insertSlides($ppt2, [1]), $ppt3);
    }

    public function testMerge() {
        $ppt1 = new PHPPowerPoint();
        $ppt2 = new PHPPowerPoint();
        $ppt3 = new PHPPowerPoint();

        $slide1 = new Slide();
        $slide2 = new Slide();
        $ppt1->setPresentation($this->presentationBuilder->create()
            ->withSlideMaster($this->slideMaster)
            ->withSlide($slide1)
            ->now());
        $ppt2->setPresentation($this->presentationBuilder->create()
            ->withSlideMaster($this->slideMaster)
            ->withSlide($slide2)
            ->now());
        $ppt3->setPresentation($this->presentationBuilder->create()
            ->withSlideMaster($this->slideMaster)
            ->withSlide($slide1)
            ->withSlide($slide2)
            ->now());

        $this->assertEquals($ppt1->merge($ppt1), $ppt3);
    }

    public function testAspose() {
        //sepcify App SID
        AsposeApp::$appSID = "d850f989-cd6b-4d5e-a788-25aba87d1c13";

        //sepcify App Key
        AsposeApp::$appKey = "73aa8a8ceaef35249928f842cd35586b";

        $strURI = "http://api.aspose.com/v1.1/slides/compressed.pptx?format=HTML";

        $signedURI = Utils::Sign($strURI);

        $responseStream = Utils::processCommand($signedURI, "GET", "", "");

        Utils::saveFile($responseStream, getcwd() . "/output.html");
    }
} 