<?php


namespace PHPPowerPoint\Tests\PHPPowerPoint\Writers;


use PHPPowerPoint\Tests\PHPPowerPointTestCase;
use QuatreCentQuatre\PHPPowerPoint\Presentation\Slide\Slide;
use QuatreCentQuatre\PHPPowerPoint\Readers\PowerPointReader;
use QuatreCentQuatre\PHPPowerPoint\Writers\PowerPointWriter;

final class PowerPointWriterTest extends PHPPowerPointTestCase
{

    public function testWrite()
    {
        $doc = PowerPointReader::read(__DIR__ . "/../../Ressources/test.pptx");
        $slide = new Slide();
        $slide->addShapeTree();
        $slide->addTextBox(
            'Helloworld !',
            array('bold'=>true)
        );
        $doc->getPresentation()->addSlide($slide);
        $this->assertTrue(PowerPointWriter::write($doc->slice(array(3, 15, 26)), __DIR__ . "/test.pptx"));
    }

} 