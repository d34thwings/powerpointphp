<?php


namespace PHPPowerPoint\Tests\PHPPowerPoint\Presentation;


use PHPPowerPoint\Tests\PHPPowerPointTestCase;
use QuatreCentQuatre\PHPPowerPoint\Exceptions\PHPPowerPointException;
use QuatreCentQuatre\PHPPowerPoint\Presentation\Builder\PresentationBuilder;
use QuatreCentQuatre\PHPPowerPoint\Presentation\Presentation;
use QuatreCentQuatre\PHPPowerPoint\Presentation\Slide\Slide;
use QuatreCentQuatre\PHPPowerPoint\Presentation\SlideLayout\SlideLayout;
use QuatreCentQuatre\PHPPowerPoint\Presentation\SlideMaster\SlideMaster;

final class PresentationTest extends PHPPowerPointTestCase {

    private $slideMaster;
    private $slideLayout;

    /** @var PresentationBuilder */
    private $presentationBuilder;

    protected function setUp() {
        $this->slideMaster = new SlideMaster("Test Master");
        $this->slideLayout = new SlideLayout("Test Layout");
        $this->presentationBuilder = new PresentationBuilder();

        $this->slideMaster->addSlideLayout($this->slideLayout);
    }

    public function testAddSlide() {
        $slide = new Slide();

        $pres = new Presentation();
        $pres->addSlide($slide);

        $this->assertEquals($pres->getSlide(0), $slide);
    }

    public function testAddSlideWithIndex() {
        $slide1 = new Slide();
        $slide2 = new Slide();

        $pres = new Presentation();
        $pres->addSlide($slide1);
        $pres->addSlide($slide2, 0);

        $this->assertEquals($pres->getSlide(0), $slide2);
    }

    public function testAddSlideWithInvalidIndex() {
        $slide1 = new Slide();
        $slide2 = new Slide();

        $pres = new Presentation();
        $pres->addSlide($slide1);
        $pres->addSlide($slide2, 3000);

        $this->assertEquals($pres->getSlide(0), $slide2);
    }

    public function testRemoveSlide() {
        $slide = new Slide();

        $pres = new Presentation();
        $pres->addSlideMaster($this->slideMaster);
        $pres->addSlide($slide);

        $pres->removeSlide(0);

        try {
            $pres->getSlide(0);
        } catch (\OutOfRangeException $e) {
            return;
        }

        $this->fail();
    }

    public function testAddSlideMaster() {
        $slide = new Slide();

        $pres = new Presentation(false);
        $pres->addSlideMaster($this->slideMaster);
        $pres->addSlide($slide);

        $this->assertEquals($pres->getSlideMasterById(0), $this->slideMaster);
    }

    public function testAddSlideMasterWithIndex() {
        $slideMaster = new SlideMaster("Test Master 2");

        $pres = new Presentation(false);
        $pres->addSlideMaster($this->slideMaster);
        $pres->addSlideMaster($slideMaster, 0);

        $this->assertEquals($pres->getSlideMasterById(0), $slideMaster);
    }

} 