<?php


namespace PHPPowerPoint\Tests\PHPPowerPoint\Presentation;


use PHPPowerPoint\Tests\PHPPowerPointTestCase;
use QuatreCentQuatre\PHPPowerPoint\Drawing\Table;
use QuatreCentQuatre\PHPPowerPoint\Presentation\GraphicFrame;
use QuatreCentQuatre\PHPPowerPoint\Presentation\Shape;
use QuatreCentQuatre\PHPPowerPoint\Presentation\ShapeTree;
use QuatreCentQuatre\PHPPowerPoint\Presentation\Slide\Slide;

final class SlideTest extends PHPPowerPointTestCase {

    public function testAddShape() {
        $slide1 = new Slide();
        $slide1->append(new ShapeTree());
        $slide2 = clone $slide1;
        $slide1->addShape();
        $slide2->getShapeTree()->append(new Shape());

        $this->assertEquals($slide1, $slide2);
    }

    public function testAddTextBox() {
        $slide1 = new Slide();
        $slide1->append(new ShapeTree());
        $slide2 = clone $slide1;
        $slide1->addTextBox("HelloWorld !");
        $txtBox = new Shape();
        $slide2->getShapeTree()->append($txtBox->addParagraph("HelloWorld !"));

        $this->assertEquals($slide1, $slide2);
    }

    public function testAddTable() {
        $slide1 = new Slide();
        $slide1->append(new ShapeTree());
        $slide2 = clone $slide1;
        $slide1->addTable(new Table(2, 3));
        $gF= new GraphicFrame();
        $slide2->getShapeTree()->append($gF->setGraphic(new Table(2, 3)));

        $this->assertEquals($slide1, $slide2);
    }
} 