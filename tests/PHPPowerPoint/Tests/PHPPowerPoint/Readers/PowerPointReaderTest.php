<?php


namespace PHPPowerPoint\Tests\PHPPowerPoint\Readers;


use PHPPowerPoint\Tests\PHPPowerPointTestCase;
use QuatreCentQuatre\PHPPowerPoint\PHPPowerPoint;
use QuatreCentQuatre\PHPPowerPoint\Readers\Exceptions\CannotReadFileException;
use QuatreCentQuatre\PHPPowerPoint\Readers\PowerPointReader;

final class PowerPointReaderTest extends PHPPowerPointTestCase {
    private $valid;
    private $invalid;

    protected function setUp() {
        $this->valid = __DIR__ . "/../../Ressources/valid.pptx";
        $this->invalid = __DIR__ . "/../../Ressources/invalid.pptx";
    }



    public function testOpenMissingFile() {
        try {
            PowerPointReader::read("missing file");
        } catch (CannotReadFileException $e) {
            return;
        }

        $this->fail();
    }

    public function testOpenInvalidFile() {
        try {
            PowerPointReader::read($this->invalid);
        } catch (CannotReadFileException $e) {
            return;
        }

        $this->fail();
    }
} 